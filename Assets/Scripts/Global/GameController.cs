﻿using UnityEngine;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	private static GlobalEventController events = null;
    private const float TEST_DELAY = 1;
    private static bool HAS_TESTED = false;

    //enum SHALALA
    //{
    //    SHA,
    //    LA
    //};

    //Dictionary<SHALALA, string> dict = new Dictionary<SHALALA,string>() {
    //        {SHALALA.SHA, "SHASHA"},
    //        {SHALALA.LA, "LALA"}
    //    };

    //Debug.Log(dict[SHALALA.SHA]);

	void Start () {

        //Debug.Log(new Vector2(0, 0) + " -> " +  GUIGlobal.native2guiPos(new Vector2(0, 0)));
        //Debug.Log(new Vector2(Screen.width, Screen.height) + " -> " +  GUIGlobal.native2guiPos(new Vector2(Screen.width, Screen.height)));

        DistanceMap = new SimpleDistanceManager();

        events = new GlobalEventController();
        Level = new LevelController();
        Input = new InputController();
        SelectionController = new SelectionController();
        Diplomacy = new PlayerController();
        EventController = new GlobalEventController();

		OnGameStart();

        
	}

    void Update()
    {
        if (!HAS_TESTED)
            if (Time.time > TEST_DELAY)
            {
                HAS_TESTED = true;
                test();
            }
        //Debug.Log(Screen.width + ", " + Screen.height + ", " + Screen.dpi + ", " + GUIGlobal.DEFAULT_HEIGHT_SM + ", " + GUIGlobal.getScreenHeightSM() + ", " + GUIGlobal.getScaleFactor() );

        Diplomacy.Update();

    }
	
	private void OnGameStart() 
	{
		Level.OnLevelStart();

        GUI.ShowResourceLabel();
	}

    #region Global Getters
    private static CameraController camera;
    private static Camera guiCamera;
    private static Map map;
    private static UnitFactory units;
    private static GUIGlobal gui;
    private static EffectController effects;
    
    public static LevelController Level;
    public static IDistanceManager DistanceMap;

    public static CameraController Camera { get {
        if (camera == null)
            camera = GameObject.Find("Main Camera").GetComponent<CameraController>();// FindObjectOfType(typeof(CameraController));
            return camera;
        } }

    public static Camera GUICamera
    {
        get
        {
            if (guiCamera == null)
                guiCamera = GameObject.Find("GUICamera").GetComponent<Camera>();// FindObjectOfType(typeof(CameraController));
            return guiCamera;
        }
    }

    public static InputController Input;

    public static Map Map { get {
            if (map == null)
                map = (Map)GameObject.FindObjectOfType(typeof(Map));
            return map;
        } }

    public static UnitFactory UnitController
    {
        get
        {
            if (units == null)
                units = (UnitFactory)GameObject.FindObjectOfType(typeof(UnitFactory));
            return units;
        }
    }

    public static GUIGlobal GUI { get {
            if (gui == null)
                gui = (GUIGlobal)GameObject.FindObjectOfType(typeof(GUIGlobal));
            return gui;
        } }

    public static EffectController Effects
    {
        get
        {
            if (effects == null)
                effects = (EffectController)GameObject.FindObjectOfType(typeof(EffectController));
            return effects;
        }
    }

    public static SelectionController SelectionController;

    public static PlayerController Diplomacy;

    public static GlobalEventController EventController;

    #endregion

    private void test()
    {
        //Debug.Log("LB = " + map.GetFreePositionAround(new Vector2(1, 1), map.GetUnitAt(new Vector2i(0, 0))));
        //Debug.Log("LT = " + map.GetFreePositionAround(new Vector2(1, 1), map.GetUnitAt(new Vector2i(0, 11))));

        //Debug.Log("RB = " + map.GetFreePositionAround(new Vector2(1, 1), map.GetUnitAt(new Vector2i(63, 0))));
        //Debug.Log("RT = " + map.GetFreePositionAround(new Vector2(1, 1), map.GetUnitAt(new Vector2i(63, 11))));


    }
}
