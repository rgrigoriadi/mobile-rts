﻿using UnityEngine;
using System.Collections;

public class LevelController {

    public LevelController()
    {

    }

	public void OnLevelStart () 
	{
        GameController.Diplomacy.InitDefault();

        GameController.SelectionController.ResetSelection();

       // GameController.Map.Loader.LoadLevel();
        GameController.Map.LoadTestLevel();
	}

    public void OnLevelEnd(Team winner)
    {
        OnLevelStart();
    }
}
