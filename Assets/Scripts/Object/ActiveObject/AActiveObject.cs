﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AActiveObject  {
    public Unit Unit;

    public AActiveObject()
    {

    }

    public abstract void Update();

    public abstract bool IsBusy();

    public abstract void Do(AUnitAction action);
}
