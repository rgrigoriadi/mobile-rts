﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimpleActiveObject : AActiveObject {

    private AUnitAction mainAction = null;

    private List<AUnitAction> secondaryActions = new List<AUnitAction>();

    public SimpleActiveObject()
    {

    }

    public override void Update()
    {
        if (IsBusy())
        {
            if (mainAction != null)
                mainAction.Act();

            foreach (AUnitAction action in secondaryActions)
                action.Act();

            ClearFinishedTasks();
        }
    }


    public void FinishAllCurrentTasks()
    {
        if (IsBusy())
        {
            if (mainAction != null)
                mainAction.Finish();

            foreach (AUnitAction action in secondaryActions)
                action.Finish();

            ClearFinishedTasks();
        }
    }

    public void ClearFinishedTasks()
    {
        if (mainAction != null)
            if (mainAction.HasFinished)
                mainAction = null;

        for (int i = 0; i < secondaryActions.Count; i++)
            i -= ClearTaskIfFinished(i) ? 1 : 0;
    }

    public bool ClearTaskIfFinished(int index)
    {
        if (secondaryActions[index].HasFinished)
        {
            secondaryActions.RemoveAt(index);
            return true;
        }

        return false;
    }


    public override bool IsBusy()
    {
        return mainAction != null;
    }

    public override void Do(AUnitAction action)
    {
        FinishAllCurrentTasks();

        if (mainAction == null)
            mainAction = action;
        else
        {
            AUnitAction leftAction = mainAction;
            mainAction = action;
            mainAction.AddPreambulaAction(leftAction);
        }
    }

    //public override void DoParallel(AUnitAction action)
    //{

    //}
}
