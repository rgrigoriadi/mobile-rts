﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AbleToMoveByPath : AAbleToMove {

    public float Speed;
    public float RotateSpeedDeg;

    public AbleToMoveByPath(float speed, float rotateSpeedDeg)
    {
        this.Speed = speed;
        this.RotateSpeedDeg = rotateSpeedDeg;
    }

    public override void Move(Vector2 mapPos)
    {
        ActiveObject.Do(new UnitActionMoveByPathFind(Unit, mapPos));//new UnitActionMoveByPath(Unit, mapPos));
    }

    public override void Move(Unit unit)
    {
        ActiveObject.Do(new UnitActionMoveToUnit(this.Unit, unit, 1.5f, this));
    }

    public override void OnFault()
    {

    }
}
