﻿using UnityEngine;
using System.Collections;

public abstract class AAbleToMove : AAbleTo, IUnitFailableAction {

    public AAbleToMove()
    {

    }

    public abstract void Move(Vector2 mapPos);

    public abstract void Move(Unit unit);



    public abstract void OnFault();
}
