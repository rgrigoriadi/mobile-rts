﻿using UnityEngine;
using System.Collections;

public class ObjectToBuild {

    public UnitFactory.UnitID ResultUID;
    public float BuildTime;
    public float BuildCost;
    public bool IsBuilding;

    public ObjectToBuild(UnitFactory.UnitID uid, float buildTime, float buildCost, bool isBuilding)
    {
        this.ResultUID = uid;
        this.BuildTime = buildTime;
        this.BuildCost = buildCost;
        this.IsBuilding = isBuilding;
    }
}
