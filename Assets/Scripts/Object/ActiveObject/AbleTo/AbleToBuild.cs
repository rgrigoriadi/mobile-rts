﻿using UnityEngine;
using System.Collections;

public class AbleToBuild : AAbleTo, ProgressBarIndicator.IProgressBarable {

    ObjectToBuild[] objectItCanBuild;

    private float buildStartTime = 0.0f;
    private float buildTime = 0.0f;

    private UnitFactory.UnitID objectBuilt;
    private float progress;

    public const bool IS_INSTANT_BUILD_MODE = true;

    public AbleToBuild(ObjectToBuild[] objectItCanBuild)
    {
        this.objectItCanBuild = objectItCanBuild;
    }

    public override void Update()
    {
        base.Update();
        if (IsBuildInProgress)
        {
            progress = (Time.time - buildStartTime) / buildTime;

            if (progress > 1.0f)
            {
                onBuildFinished(objectBuilt);
                objectBuilt = UnitFactory.UnitID.NONE;
                buildStartTime = 0.0f;
            }
        }
        else
            progress = 0.0f;
    }

    public void Build(UnitFactory.UnitID uid)
    {


        ObjectToBuild buildParams = GameController.UnitController.BUILD_PARAMS_BY_UID()[uid];

        //if (buildParams.IsBuilding)
        //{
        //    Debug.LogWarning("Need to run position picker when building a building");
        //    GameController.Input.AddLowestChild(new InputPickBuildPositionHandler(GameController.Input.CurrentHandler, uid, Unit));
        //}
        //else
        StartBuildingProcess(uid, buildParams.BuildTime, buildParams.BuildCost);
    }

    public void StartBuildingProcess(UnitFactory.UnitID objectToBuild, float buildTime, float buildCost)
    {
        // TODO вычтем бабло
        Unit.Player.GetResourceController().Substract(buildCost);

        buildStartTime = Time.time;
        this.buildTime = IS_INSTANT_BUILD_MODE ? 0.5f : buildTime;
        this.objectBuilt = objectToBuild;
    }

    private void onBuildFinished(UnitFactory.UnitID uid)
    {
        Vector2i unitSize = GameController.UnitController.SIZE_BY_UID()[uid];
        Vector2i freePos = GameController.Map.GetFreePositionAround(this.Unit.Position.BottomLeftOnMapMap, this.Unit.Size, unitSize, false);//(unitSize, this.Unit);

        GameController.UnitController.CreateObject(uid, freePos, this.Unit.Player, null);
        // GameController.UnitController.InstantiateObject(objectBuilt, GameController.Map.MapCell(Unit.Position.CenterOnMap), Unit.Player); // TODO FCK YOU

        Debug.Log(this.Unit.name + " will place " + uid + " at " + freePos.ToString());
    }

    public ObjectToBuild[] GetObjectsCanBuild()
    {
        return objectItCanBuild;
    }

    public bool IsBuildInProgress
    {
        get
        {
            return objectBuilt != UnitFactory.UnitID.NONE;
        }
    }

    public float GetProgress()
    {
        return progress > 1.0f ? 1.0f : progress;
    }

    public bool WillShow()
    {
        return progress > 0.0f;
    }
}
