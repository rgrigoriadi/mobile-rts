﻿using UnityEngine;
using System.Collections;

public abstract class AAbleToAtack : AAbleTo {
    public AAtackEventListener AtackListener;

    public AAbleToAtack(AAtackEventListener ael)
    {
        this.AtackListener = ael;
    }

    public override void Update()
    {
        base.Update();
        AtackListener.Update();
    }

    public abstract void Atack(HP target, bool noMoveMode);

    public abstract void Atack(Vector2 mapPos);

    public abstract bool IsAtacking();

    public abstract float AtackRange();
}
