﻿using UnityEngine;
using System.Collections.Generic;

public class AbleToMine : ProgressBarIndicator.IProgressBarable
{

    public Unit Unit;
    public float MineSpeed; // units per second
    private float captureRange;

    private const bool isDebug = true;

    private enum captureState { NOT_CAPTURED, CAPTURING, CAPTURED };
    private enum captureDirection { CAPTURE, PAUSE, LOOSE, NULL };
    private captureState state = captureState.NOT_CAPTURED;
    private captureDirection dir = captureDirection.NULL;
    private Player playerCapturing;
    private float captureProgress;

    private Player currentPlayer = null;

    public AbleToMine(float mineSpeed, float captureRange) // Initial player is set throwgh Unit.Player field and later is set to neutral. currentPlayer field determines it then.
    {
        this.MineSpeed = mineSpeed;
        this.captureRange = captureRange;
    }

    public void Update()
    {
        if (currentPlayer != null)
        {
            float amount = MineSpeed * Time.deltaTime;
            currentPlayer.GetResourceController().Add(amount);

            List<Player> playersInRange = getPlayersInRange();

            updateState(playersInRange);
            updateCaptureProgress();
        }
        else
        {
            currentPlayer = Unit.Player;
            Unit.Player = PlayerController.DEFAULT_NEUTRAL;
            if (currentPlayer != PlayerController.DEFAULT_NEUTRAL)
                state = captureState.CAPTURED;
        }
    }

    private void updateState(List<Player> playersInRange)
    {
        
        if (state == captureState.NOT_CAPTURED)
        {
            if (playersInRange.Count >= 2)
                ;
            else if (playersInRange.Count == 1)
            {
                Player onlyPlayerInRange = playersInRange[0];
                state = captureState.CAPTURING;
                dir = captureDirection.CAPTURE;
                playerCapturing = onlyPlayerInRange;
                if (isDebug)
                    Debug.Log("Mine Reporting: CAPTURING STARTED: I belong to " + currentPlayer.Name);
            }
        }
        else if (state == captureState.CAPTURING)
        {
            if (playersInRange.Count >= 2)
                if (currentPlayer == PlayerController.DEFAULT_NEUTRAL)
                    if (!playersInRange.Contains(playerCapturing))
                        dir = captureDirection.LOOSE;
                    else
                        dir = captureDirection.PAUSE;
                else
                    if (!playersInRange.Contains(currentPlayer))
                        dir = captureDirection.LOOSE;
                    else
                        dir = captureDirection.PAUSE;
                else if (playersInRange.Count == 1)
                {
                    Player onlyPlayerInRange = playersInRange[0];
                    if (currentPlayer == PlayerController.DEFAULT_NEUTRAL)
                        if (onlyPlayerInRange == playerCapturing)
                            dir = captureDirection.CAPTURE;
                        else
                            dir = captureDirection.LOOSE;
                    else
                        if (onlyPlayerInRange == currentPlayer)
                            dir = captureDirection.CAPTURE;
                        else
                            dir = captureDirection.LOOSE;
                }
                else
                {
                    if (currentPlayer == PlayerController.DEFAULT_NEUTRAL)
                        dir = captureDirection.LOOSE;
                    else
                        dir = captureDirection.CAPTURE;
                }
        }
        else if (state == captureState.CAPTURED)
        {
            if (playersInRange.Count > 0 && !playersInRange.Contains(currentPlayer))
            {
                state = captureState.CAPTURING;
                dir = captureDirection.LOOSE;

                if (isDebug)
                    Debug.Log("Mine Reporting: LOOSING STARTED: I belong to " + currentPlayer.Name);
            }
        }
    }

    private void updateCaptureProgress()
    {
        if (state == captureState.CAPTURING)
            if (dir == captureDirection.CAPTURE)
            {
                captureProgress += getCaptureSpeed() * Time.deltaTime;
                if (captureProgress >= 1.0f)
                {
                    state = captureState.CAPTURED;
                    dir = captureDirection.NULL;
                    captureProgress = 1.0f;

                    if (currentPlayer == PlayerController.DEFAULT_NEUTRAL)
                        currentPlayer = playerCapturing;
					playerCapturing = null;

                    if (isDebug)
                        Debug.Log("Mine Reporting: CAPTURING FINISHED: I belong to " + currentPlayer.Name);
                }
            }
            else if (dir == captureDirection.LOOSE)
            {
                captureProgress -= getCaptureSpeed() * Time.deltaTime;
                if (captureProgress <= 0.0f)
                {
                    state = captureState.NOT_CAPTURED;
                    dir = captureDirection.NULL;
                    captureProgress = 0.0f;
                    playerCapturing = null;

                    if (currentPlayer != PlayerController.DEFAULT_NEUTRAL)
                        currentPlayer = PlayerController.DEFAULT_NEUTRAL;

                    if (isDebug)
                        Debug.Log("Mine Reporting: LOOSING FINISHED: I belong to " + currentPlayer.Name);
                }
            }
    }

    private float getCaptureSpeed()
    {
        return 0.3f;
    }

    private List<Player> getPlayersInRange()
    {
        List<Player> ret = new List<Player>();

        Unit[] unitsInRange = GameController.DistanceMap.GetAnyUnitsNonNeutralInRangeOf(Unit, captureRange);

        for (int i = 0; i < unitsInRange.Length; i++)
        {
            Player player = unitsInRange[i].Player;
            if (player != PlayerController.DEFAULT_NEUTRAL)
                if (!ret.Contains(player))
                    ret.Add(player);
        }

        return ret;
    }

    public float GetProgress()
    {
        return captureProgress;
    }

    public bool WillShow()
    {
        return captureProgress != 0.0f && captureProgress != 1.0f;
    }
}
