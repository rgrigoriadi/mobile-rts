﻿using UnityEngine;
using System.Collections;

public class AbleToAtackInstant : AAbleToAtack {

    public float Range;
    public float ReloadTime;
    public float DamagePerHit;

    public const float NEVER = -1.0f;
    public float lastAtackTime = NEVER;

    public AbleToAtackInstant(AAtackEventListener ael, float range, float reloadTime, float damage)
        : base(ael)
    {
        this.Range = range;
        this.ReloadTime = reloadTime;
        this.DamagePerHit = damage;
    }

    public override void Atack(HP target, bool holdPosition)
    {
        if (target == null)
        {
            Debug.LogError("Can not atack, target has no HP controller!");
            return;
        }

        if (Unit != target.Unit)
        {
            ActiveObject.Do(new UnitActionAtackInstant(Unit, target, holdPosition));
        }
    }

    public override void Atack(Vector2 mapPos)
    {
        Debug.Log("Can only atack other unit");
    }

    public override bool IsAtacking()
    {
        throw new System.NotImplementedException();
    }

    public override float AtackRange()
    {
        return Range;
    }
}
