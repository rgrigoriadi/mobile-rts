﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HP : AAbleTo, ProgressBarIndicator.IProgressBarable
{
    public float MaxHitPoints;
    public float CurrentHitPoints;
    public float RegenSpeed;

    public HP(float maxHP, float hp, float regenSpeed)
    {
        this.MaxHitPoints = maxHP;
        this.CurrentHitPoints = hp;
        this.RegenSpeed = regenSpeed;
    }
    
    public bool IsUnitDead
    {
        get
        {
            if (CurrentHitPoints <= 0)
                return true;
            else
                return false;
        }
    }


    public void OnAttack(float damage)
    {
        CurrentHitPoints -= damage;

        if (IsUnitDead)
            Unit.DestroyUnit();
    }

    public override void Update()
    {
        CurrentHitPoints += RegenSpeed * Time.deltaTime;
    }

    public float GetHPPercents()
    {
        return CurrentHitPoints / MaxHitPoints;
    }

    public float GetProgress()
    {
        return CurrentHitPoints / MaxHitPoints;
    }

    public bool WillShow()
    {
        return CurrentHitPoints != MaxHitPoints;
    }
}
