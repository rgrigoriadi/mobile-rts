﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitActionMoveToUnit : AUnitAction, IUnitFailableAction
{

    private MoveSequence path;
    private Unit target;
    private Vector2i targetPosition;
    private float radius;
    private IUnitFailableAction parent;

    private bool faultFlag = false;

    public UnitActionMoveToUnit(Unit unit, Unit target, float radius, IUnitFailableAction parent)
        : base(unit)
    {
        Debug.Log("Move to unit action added");

        this.target = target;
        this.targetPosition = target.Position.BottomLeftOnMapMap;
        this.radius = radius;
        this.parent = parent;

        initPath();
    }

    protected override void performAction()
    {
        //throw new System.NotImplementedException();

        //Debug.Log("[Action Perform] Move by path, path length = " + path.Length);

        if (path.IsEmpty)
            onFinish();
        else
        {
            if (faultFlag || !target.Position.BottomLeftOnMapMap.Equals(targetPosition))
            {
                faultFlag = false;
                initPath();
                targetPosition = target.Position.BottomLeftOnMapMap;
                performAction();
            }
            else
            {
                secondaryTask = new UnitActionMoveOneCell(unit, path.CurrentMove.ToVector2i, this);
                path.SwitchNextPoint();
            }

        }

    }

    private void initPath()
    {
        path = new MoveSequence();
        List<Vector2i> pathPoints = MapAnalyser.FindPath(GameController.Map, unit, target, radius, unit);

        if (pathPoints != null)
        {
            path.SetPath(pathPoints);
        }
        else
        {
            Debug.LogWarning("Path for " + unit.name + " to " + target.Name + "(" + target.Position.BottomLeftOnMapMap + ") not found!");
            onFinish();
            parent.OnFault();
        }
    }

    protected override void tryToFinish()
    {
        path.Clear();
    }

    public void OnFault()
    {
        faultFlag = true;
    }
}
