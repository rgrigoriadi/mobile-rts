﻿using UnityEngine;
using System.Collections;

public class UnitActionMoveOneCell : AUnitAction
{
    private MoveDirection direction;

    private Vector2 positionBeforeMove;
    private IUnitFailableAction parent;

    public AbleToMoveByPath AbleToMove
    {
        get
        {
            return (AbleToMoveByPath) unit.AbleToMove;
        }
    }

    public UnitActionMoveOneCell(Unit unit, Vector2i direction, IUnitFailableAction parent)
        : base(unit)
    {
        this.direction = new MoveDirection(direction);
        this.parent = parent;
    }

    protected override void performAction()
    {
        //Debug.Log("[Action Perform] Move one cell, distance = " + direction.Distance);

        if (!direction.HasStartedMove)
        {
            positionBeforeMove = unit.Position.BottomLeftOnMapMap.ToVector2;
            Vector2i nextPos = unit.Position.BottomLeftOnMapMap + direction.ToVector2i;

            if (!GameController.Map.IsInBounds(nextPos, unit.Size))
            {
                Debug.LogError("Why so bad???? unit " + unit.Name + " pos " + unit.Position.BottomLeftOnMapMap + "/" + unit.Position.BottomLeftOnMapReal + " dir = " + direction.ToVector2i + " = " + nextPos);
            }

            if (GameController.Map.IsBusy(nextPos, unit.Size, unit))
            {
                parent.OnFault();
                onFinish();
                return;
            }

            GameController.Map.PutUnitShifted(direction.ToVector2i, unit);
            unit.Position.BottomLeftOnMapMap = nextPos;
        }

        direction.Distance += AbleToMove.Speed * Time.deltaTime;

        updatePosition();

        if (direction.HasFinishedMove)
        {

            GameController.Map.RemoveUnitFromCell(new Vector2i(positionBeforeMove));

            positionBeforeMove = unit.Position.BottomLeftOnMapReal;

            GameController.DistanceMap.UpdateFor(unit);

            onFinish();
        }


    }

    private void updatePosition()
    {
        unit.Position.BottomLeftOnMapReal = positionBeforeMove + direction.PosAfterMove(direction.Distance);
    }

    protected override void tryToFinish()
    {

    }
}
