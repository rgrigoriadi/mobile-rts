﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitActionMoveByPathFind : AUnitAction, IUnitFailableAction
{

    private MoveSequence path;
    private Vector2i targetCell;

    private bool faultFlag = false;

    public UnitActionMoveByPathFind(Unit unit, Vector2 mapPos)
        : base(unit)
    {
        //Debug.Log("Move action added");

        targetCell = GameController.Map.MapCell(mapPos);

        initPath();

        //Debug.Log("Path found, len = " + path.Length);
    }

    protected override void performAction()
    {
        //Debug.Log("[Action Perform] Move by path, path length = " + path.Length);

        if (path.IsEmpty)
            onFinish();
        else
        {
            if (faultFlag)
            {
                faultFlag = false;
                initPath();

                performAction();
            }
            else
            {
                secondaryTask = new UnitActionMoveOneCell(unit, path.CurrentMove.ToVector2i, this);
                path.SwitchNextPoint();
            }
      
        }

    }

    private void initPath()
    {
        path = new MoveSequence();
        List<Vector2i> pathPoints = MapAnalyser.FindPath(GameController.Map, unit.Position.BottomLeftOnMapMap, targetCell, unit);

        if (pathPoints != null)
        {
            path.SetPath(pathPoints);
        }
        else
        {
            Debug.LogWarning("Path for " + unit.name + " to " + targetCell + " not found!");
            onFinish();
        }
    }

    protected override void tryToFinish()
    {
        path.Clear();
    }

    public void OnFault()
    {
        faultFlag = true;
    }
}
