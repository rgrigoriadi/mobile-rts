﻿using UnityEngine;
using System.Collections;

public class UnitActionAtackInstant : AUnitActionAtack, IUnitFailableAction {

    private HP target;

    private bool holdPosition;

    public UnitActionAtackInstant(Unit unit, HP target, bool holdPosition)
        : base(unit)
    {
        this.target = target;
        this.holdPosition = holdPosition;
    }

    protected override void performAction()
    {
        if (target.Unit == null || target.IsUnitDead)
        {
            onFinish();
            return;
        }

        if (DistanceToTarget > AbleToAtackInstant.Range)
        {
            if (!holdPosition)
                //throw new System.NotImplementedException();
                secondaryTask = new UnitActionMoveToUnit(unit, target.Unit, AbleToAtackInstant.AtackRange(), this);
            else
                tryToFinish();
        }

        if (DistanceToTarget > AbleToAtackInstant.Range) // TODO Rotation check  || (false))
        {
            // DoNothing
        }
        else if (AbleToAtackInstant.lastAtackTime == AbleToAtackInstant.NEVER || (Time.time > AbleToAtackInstant.lastAtackTime + AbleToAtackInstant.ReloadTime))
        {
            ((Transform)GameObject.Instantiate(GameController.Effects.AttackPrefab)).GetComponent<LineShootEffect>().show(unit.transform.position, target.Unit.transform.position);

            target.OnAttack(AbleToAtackInstant.DamagePerHit);
            //Debug.Log("[Action] " + unit.Name + " atacks " + target.Unit.Name + " " + target.CurrentHitPoints + " hp left");
            AbleToAtackInstant.lastAtackTime = Time.time;
        }
        else
        {
            // DONOTHING
        }
    }

    protected override void tryToFinish()
    {
        onFinish();
    }

    private float DistanceToTarget
    {
        get
        {
            return GameController.DistanceMap.GetRangeBetween(unit, target.Unit);
        }
    }

    protected AbleToAtackInstant AbleToAtackInstant
    {
        get
        {
            return (AbleToAtackInstant) unit.AbleToAtack;
        }
    }


    public void OnFault()
    {
        onFinish();

        Debug.LogWarning("Cannot atack, path not found");
    }
}
