﻿using UnityEngine;
using System.Collections;

public interface IUnitFailableAction {
    void OnFault();
}
