﻿using UnityEngine;
using System.Collections;

public abstract class AUnitAction {

    protected AUnitAction secondaryTask;

    private bool hasFinished = false;

    protected Unit unit;

    public AUnitAction(Unit unit)
    {
        this.unit = unit;
    }

    public void Act () 
    {
        if (secondaryTask != null)
            if (!secondaryTask.HasFinished)
            {
                secondaryTask.Act();
            }
            else 
            {
                secondaryTask = null;
                performAction();
            }
        else if (!hasFinished) 
        {
            performAction();
        }
    }

    protected abstract void performAction();

    public void AddPreambulaAction(AUnitAction action) {
        if (secondaryTask == null)
            secondaryTask = action;
        else
            secondaryTask.AddPreambulaAction(action);
    }

    public bool HasFinished 
    {
        get
        {
            return hasFinished && (secondaryTask == null || secondaryTask.hasFinished);
        }
    }

    public void Finish() 
    {
        if (secondaryTask != null)
            secondaryTask.tryToFinish();

        tryToFinish();
    }

    protected void onFinish()
    {
        //Debug.Log("onFinish call by " + this.GetType().Name);
        hasFinished = true;
    }

    protected abstract void tryToFinish();
}
