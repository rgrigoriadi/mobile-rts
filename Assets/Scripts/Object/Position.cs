﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Position {
    private GameObject obj;
    private Vector2i size;

    private Vector2i bottomLeftOnMap;

    public Position(GameObject obj, Vector2i size)
    {
        this.obj = obj;
        this.size = size;
    }

    public void UpdateSize(Vector2i newSize)
    {
        this.size = newSize;
    }
    
    public Vector2i BottomLeftOnMapMap
    {
        get { return bottomLeftOnMap; }
        set { bottomLeftOnMap = value; }
    }
    public Vector2i TopRightOnMapMap
    {
        get { return BottomLeftOnMapMap + size; }
        set { BottomLeftOnMapMap = value - size; }
    }


    public Vector2 CenterOnMapReal
    {
        get { return GameController.Map.WorldToMap(obj.transform.position); }

        set
        {
            Vector2 pos = GameController.Map.MapToWorld(value);
            float height = GameController.Map.HeightAt(value) - obj.transform.GetChild(0).GetComponent<MeshRenderer>().bounds.size.z / 2.0f;
            obj.transform.position = new Vector3(pos.x, pos.y, height);
        }
    }

    public Vector2 BottomLeftOnMapReal
    {
        get { return CenterOnMapReal - size / 2.0f; }
        set { CenterOnMapReal = value + size / 2.0f; }
    }

    public Vector2 TopRightOnMapReal
    {
        get { return CenterOnMapReal + size / 2.0f; }
        set { CenterOnMapReal = value - size / 2.0f; }
    }

    public List<Vector2i> GetOccupiedCellsList() // TODO include shifted when move
    {
        List <Vector2i> cells = new List<Vector2i>();

        for (int i = 0; i < size.x; i++)
            for (int j = 0; j < size.y; j++)
                cells.Add(BottomLeftOnMapMap + new Vector2i(i, j));
        return cells;   
    }
}
