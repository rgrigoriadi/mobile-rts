﻿using UnityEngine;
using System.Collections;

public class BuildStatePrefab : ProgressBarIndicator.IProgressBarable {

    private float BuildTime = float.NaN;
    private float BuildStartTime = float.NaN;

    private float progress = 0.0f;
    private Unit unit;

    private UnitFactory.UnitID uid;

    public const bool IS_INSTANT_BUILD_MODE = true;

    public BuildStatePrefab()
    {

    }

    public void Init(Unit unit, float buildTime, UnitFactory.UnitID idToBuild)
    {
        this.unit = unit;
        BuildStartTime = Time.time;
        BuildTime = IS_INSTANT_BUILD_MODE ? 0.5f : buildTime;
        this.uid = idToBuild;
    }

	public void Update () {
        if (float.IsNaN(BuildStartTime) || float.IsNaN(BuildTime))
        {
            Debug.LogError("BuildStatePrefab has been created but was not initialized");
            return;
        }

        unit.HPController.CurrentHitPoints += (GetBuildProgress() - progress) * unit.HPController.MaxHitPoints;
        progress = GetBuildProgress();
        if (progress >= 1.0f)
            onBuildFinished();
	}

    private void onBuildFinished()
    {
        // throw new System.NotImplementedException();

        // Remove build component
        GameObject oldImage = unit.gameObject.transform.GetChild(0).gameObject;
        oldImage.transform.parent = null;
        GameObject.Destroy(oldImage);
        unit.BuildState = null;

        // Add all other components

        GameObject imageGo = ((Transform)GameObject.Instantiate(GameController.UnitController.PREFABS_BY_UIDS()[uid], Vector3.zero, Quaternion.identity)).gameObject;
        imageGo.transform.parent = unit.gameObject.transform;
        imageGo.transform.localPosition = Vector3.zero;
        imageGo.transform.localScale = new Vector3(1, 1, 1);
        imageGo.transform.Rotate(new Vector3(0, 0, 1), 180.0f);

        UnitFactory.UnitInitParams parampampams = GameController.UnitController.UNIT_PARAMS_BY_UID()[uid];
        unit.MustDownScale = parampampams.MustDownScale;
        unit.gameObject.name = parampampams.Name;
        unit.name = parampampams.Name;
        unit.Name = parampampams.Name;

        unit.Size = GameController.UnitController.SIZE_BY_UID()[uid];

        unit.Init(unit.Player, uid);

        UnitFactory.UnitFactoryGameObject.SetAll(unit, GameController.UnitController.COMPONENTS_BY_UID()[uid]);

        unit.InitComponentsFinally();
        // Update gui 4 unit
        // TODO
    }

    public float GetBuildProgress()
    {
        float progress = (Time.time - BuildStartTime) / BuildTime;
        return progress > 1.0f ? 1.0f : progress;
    }


    public float GetProgress()
    {
        return GetBuildProgress();
    }

    public bool WillShow()
    {
        return true;
    }
}
