﻿using UnityEngine;
using System.Collections;

public class SimpleSelectable : ATapable {
    private bool isSelected = false;

    public bool IsSelected { get { return isSelected; } }

    public override void Select()
    {
        if (!isSelected) {

            GameController.SelectionController.OnSelect(this);

            isSelected = true;

            Unit.GUI.ShowGUI(true);
        }
    }

    public override void UnSelect()
    {
        if (isSelected)
        {
            GameController.SelectionController.OnUnSelect(this);

            isSelected = false;

            Unit.GUI.HideGUI();
        }
    }
}
