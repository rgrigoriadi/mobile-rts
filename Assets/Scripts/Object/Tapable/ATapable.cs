﻿using UnityEngine;
using System.Collections;

public abstract class ATapable : MonoBehaviour {

    public Unit Unit
    {
        get
        {
            return GetComponent<Unit>();
        }
    }

    public abstract void Select();
    
    public abstract void UnSelect();

}
