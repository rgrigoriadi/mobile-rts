﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitSelection {

    private List<ATapable> container;

    public List<Unit> List
    {
        get
        {
            List<Unit> units = new List<Unit>();

            foreach (SimpleSelectable sel in container)
                units.Add(sel.Unit);

            return units;
        }
    }

    public List<ATapable> SelectableList
    {
        get
        {
            return container;
        }
    }

    public UnitSelection(List<ATapable> units)
    {
        container = new List<ATapable>();
        container.AddRange(units);
    }

    public bool IsAbleToMove
    {
        get
        {
            foreach (SimpleSelectable unit in container)
                if (unit.Unit.AbleToMove != null)
                    return true;
            return false;
        }
    }

    public bool IsAbleToAtack
    {
        get
        {
            foreach (SimpleSelectable unit in container)
                if (unit.Unit.AbleToAtack != null)
                    return true;
            return false;
        }
    }

    public int Count { get { return container.Count; } }


}
