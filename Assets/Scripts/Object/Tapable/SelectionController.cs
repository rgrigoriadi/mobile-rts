﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectionController {

    List<ATapable> selected = new List<ATapable>();

	void Start () {
        
	}
	
	void Update () {
	
	}

    public void ResetSelection()
    {
        List<ATapable> selected = GetSelectedUnits().SelectableList;

        foreach (SimpleSelectable sel in selected)
            sel.UnSelect();

        OnSelectionChange();
    }

    public UnitSelection GetSelectedUnits()
    {
        //SimpleSelectable[] selectable = getSelectableUnits();
        //List<SimpleSelectable> ret = new List<SimpleSelectable>();

        //foreach (SimpleSelectable sel in selectable)
        //    if (sel.IsSelected)
        //        ret.Add(sel);

        //return new UnitSelection(ret);

        return new UnitSelection(selected);
    }

    private SimpleSelectable[] getSelectableUnits()
    {
        return (SimpleSelectable[])GameObject.FindObjectsOfType(typeof(SimpleSelectable));
    }

    public void OnSelect(ATapable selectable)
    {
        selected.Add(selectable);

        //Debug.LogWarning("Select " + selected.Count + " in collection");

        OnSelectionChange();
    }

    public void OnUnSelect(ATapable selectable)
    {
        selected.Remove(selectable);

        //Debug.LogWarning("Unselect " + selected.Count + " in collection");

        if (selected.Count == 0)
        {
            OnUnselectedAll();
        }

        OnSelectionChange();
    }

    private void OnUnselectedAll()
    {
        GameController.Input.RemoveAllHandlers();
        OnSelectionChange();
    }

    private void OnSelectionChange()
    {
        GameController.GUI.OnSelectionChange();
    }
}
