﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Unit : MonoBehaviour
{
    public string Name;
    private UnitFactory.UnitID uid;
    public UnitFactory.UnitID UID { get { return uid; } }

    public Vector2i Size {
        get { return size; } 
        set { size = value; if (Position != null) Position.UpdateSize(size);} 
    }

    private Vector2i size;
    public Color Color;
    public bool MustDownScale = true;
    private float downscaleCoef = 0.87f;

    public bool IsTestObject = true;
    private bool isOnMap;
    public bool IsOnMap()
    {
        return isOnMap && gameObject.activeSelf;
    }

    public Position Position;
    private Player player;
    public Player Player
    {
        get
        {
            return player;
        }
        set
        {
            if (player != null)
                player.OnRemoveUnit();
            player = value;
            player.OnAddUnit();
        }
    }

    public AActiveObject ActiveObject;
    public AAbleToAtack AbleToAtack;
    public AAbleToMove AbleToMove;
    public AbleToBuild AbleToBuild;
    public HP HPController;
    public BuildStatePrefab BuildState;
    public AbleToMine Mine;

    public GUIForUnitLocalHandler GUI
    {
        get
        {
            GUIForUnitLocalHandler gui = GetComponent<GUIForUnitLocalHandler>();

            if (gui == null) {
                gui = gameObject.AddComponent<GUIForUnitLocalHandler>();
            }

            return gui;
        }
    }

    void Awake()
    {

    }

    public void InitComponentsFinally()
    {


        if (ActiveObject != null)
            ActiveObject.Unit = this;

        if (AbleToAtack != null)
        {
            AbleToAtack.Unit = this;
            AbleToAtack.AtackListener.Unit = this;
        }

        if (AbleToMove != null)
            AbleToMove.Unit = this;

        if (AbleToBuild != null)
            AbleToBuild.Unit = this;

        if (HPController != null)
            HPController.Unit = this;

        if (Mine != null)
            Mine.Unit = this;


    }

    void Start()
    {
        if (IsTestObject)
        {
            GameObject.Destroy(gameObject);
            return;
        }
        isOnMap = true;
        Player.OnAddUnit();

        if (!IsTestObject && Player == null)
        {
            Debug.LogError("Player for " + Name + " was not set!");
        }

    }

    void Update()
    {
        if (ActiveObject != null)
            ActiveObject.Update();
        if (AbleToAtack != null)
            AbleToAtack.Update();
        if (AbleToMove != null)
            AbleToMove.Update();
        if (AbleToBuild != null)
            AbleToBuild.Update();
        if (HPController != null)
            HPController.Update();
        if (BuildState != null)
            BuildState.Update();
        if (Mine != null)
            Mine.Update();
    }

    public SimpleSelectable Select // TODO REMOVE SELECT FROM MODEL
    {
        get
        {
            return GetComponent<SimpleSelectable>();
        }
    }

    public void DestroyUnit()
    {
        isOnMap = false;

        //Debug.Log("Destroying " + gameObject.name);

        if (Select != null)
            Select.UnSelect();

        GameController.Map.RemoveUnit(this);
        GameController.DistanceMap.OnRemove(this);
        Player.OnRemoveUnit();
        GameObject.Destroy(gameObject);
    }

    public string GetName()
    {
        return Name;
    }

    public bool IsSelected
    {
        get
        {
            SimpleSelectable selectable = Select;
            return selectable == null || selectable.IsSelected;
        }
    }

    public class GUIHelper
    {
        public Vector3 GetUnitOnScreenTop(Unit unit)
        {
            Vector3 pos = new Vector3(unit.transform.position.z, unit.transform.position.y, unit.GetComponent<MeshRenderer>().bounds.max.z);
            return GameController.Camera.camera.WorldToScreenPoint(pos);
        }
    }

    public void Init(Player player, UnitFactory.UnitID uid)
    {
        if (MustDownScale)
            transform.GetChild(0).localScale = transform.GetChild(0).localScale * downscaleCoef;

        this.Player = player;
        this.uid = uid;

        transform.localScale = new Vector3(Size.x, Size.y, 1);
    }

}
