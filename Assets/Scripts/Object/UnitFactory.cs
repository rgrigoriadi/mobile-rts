﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitFactory : MonoBehaviour {

    public enum UnitID
    {
        NONE,

        STONE,
        ENERGY_POINT,

        MARINE,
        MERCENARY,
        APC,
        LIGHT_ASSAULT_BOT,
        WARDOG,
        TANK,
        PROJECT_GRAVITY,
        HELI,
        QUADROCOPTER,

        BARRACKS,
        MECH_BAY,
        FACTORY,
        TZEH,
        FIELD_LAB,
        HANGAR,

        BUILD_BARRACKS,
        BUILD_MECH_BAY,
        BUILD_FACTORY,
        BUILD_TZEH,
        BUILD_FIELD_LAB,
        BUILD_HANGAR
    }
    
    public Transform StonePrefab;
    public Transform EnergyPointPrefab;

    public Transform MarinePrefab;
    public Transform MercenaryPrefab;
    public Transform APCPrefab;
    public Transform LightAssaultBotPrefab;
    public Transform WardogPrefab;
    public Transform TankPrefab;
    public Transform ProjectGravityPrefab;
    public Transform HeliPrefab;
    public Transform QuadrocopterPrefab;

    public Transform BarracksPrefab;
    public Transform MechBayPrefab;
    public Transform FactoryPrefab;
    public Transform TzehPrefab;
    public Transform FieldLabPrefab;
    public Transform HangarPrefab;

    public Transform BuildBarracksPrefab;
    public Transform BuildMechBayPrefab;
    public Transform BuildFactoryPrefab;
    public Transform BuildTzehPrefab;
    public Transform BuildFieldLabPrefab;
    public Transform BuildHangarPrefab;

    public Transform GhostBarracksPrefab;
    public Transform GhostMechBayPrefab;
    public Transform GhostFactoryPrefab;
    public Transform GhostTzehPrefab;
    public Transform GhostFieldLabPrefab;
    public Transform GhostHangarPrefab;

    public Dictionary<UnitID, UnitID> BUILD_UIDS_BY_UIDS()
    {
        return new Dictionary<UnitID, UnitID>{
            {UnitID.BARRACKS, UnitID.BUILD_BARRACKS},
            {UnitID.MECH_BAY, UnitID.BUILD_MECH_BAY},
            {UnitID.FACTORY, UnitID.BUILD_FACTORY},
            {UnitID.TZEH, UnitID.BUILD_TZEH},
            {UnitID.FIELD_LAB, UnitID.BUILD_FIELD_LAB},
            {UnitID.HANGAR, UnitID.BUILD_HANGAR}
        };
    }

    public Dictionary<UnitID, UnitID> UIDS_BY_BUILD_UIDS()
    {
        return new Dictionary<UnitID, UnitID>{
            {UnitID.BUILD_BARRACKS, UnitID.BARRACKS},
            {UnitID.BUILD_MECH_BAY, UnitID.MECH_BAY},
            {UnitID.BUILD_FACTORY,  UnitID.FACTORY},
            {UnitID.BUILD_TZEH,     UnitID.TZEH},
            {UnitID.BUILD_FIELD_LAB,UnitID.FIELD_LAB},
            {UnitID.BUILD_HANGAR,   UnitID.HANGAR}
        };
    }

    public Dictionary<UnitID, UnitInitParams> UNIT_PARAMS_BY_UID()
    {
        return new Dictionary<UnitID, UnitInitParams>() {
            {UnitID.STONE, new UnitInitParams("Stone")},
            {UnitID.ENERGY_POINT, new UnitInitParams("Enery Mine")},

            {UnitID.MARINE, new UnitInitParams("Marine Squad")},
            {UnitID.MERCENARY, new UnitInitParams("Mercenaries Squad")},
            {UnitID.APC, new UnitInitParams("APC")},
            {UnitID.LIGHT_ASSAULT_BOT, new UnitInitParams("Light Bot")},
            {UnitID.WARDOG, new UnitInitParams("WarDog")},
            {UnitID.TANK, new UnitInitParams("Tank")},
            {UnitID.PROJECT_GRAVITY, new UnitInitParams("Project G")},
            {UnitID.HELI, new UnitInitParams("Heli")},
            {UnitID.QUADROCOPTER, new UnitInitParams("QuadroCopter")},

            {UnitID.BARRACKS, new UnitInitParams("Barracks")},
            {UnitID.MECH_BAY, new UnitInitParams("Mech Bay")},
            {UnitID.FACTORY, new UnitInitParams("Factory")},
            {UnitID.TZEH, new UnitInitParams("Tzeh")},
            {UnitID.FIELD_LAB, new UnitInitParams("Field Lab")},
            {UnitID.HANGAR, new UnitInitParams("Hangar")},

            {UnitID.BUILD_MECH_BAY, new UnitInitParams("Command Center in build")},
            {UnitID.BUILD_BARRACKS, new UnitInitParams("Barracks in build")},
            {UnitID.BUILD_FACTORY, new UnitInitParams("Factory in build")},
            {UnitID.BUILD_TZEH, new UnitInitParams("Heavy Factory in build")},
            {UnitID.BUILD_FIELD_LAB, new UnitInitParams("Field Lab in build")},
            {UnitID.BUILD_HANGAR, new UnitInitParams("Hangar in build")}
        };
    }

    public Dictionary<UnitID, Transform> PREFABS_BY_UIDS()
    {
        return new Dictionary<UnitID, Transform>{
            {UnitID.STONE, StonePrefab},
            {UnitID.ENERGY_POINT, EnergyPointPrefab},

            {UnitID.MARINE, MarinePrefab},
            {UnitID.MERCENARY, MercenaryPrefab},
            {UnitID.APC, APCPrefab},
            {UnitID.LIGHT_ASSAULT_BOT, LightAssaultBotPrefab},
            {UnitID.WARDOG, WardogPrefab},
            {UnitID.TANK, TankPrefab},
            {UnitID.PROJECT_GRAVITY, ProjectGravityPrefab},
            {UnitID.HELI, HeliPrefab},
            {UnitID.QUADROCOPTER, QuadrocopterPrefab},

            {UnitID.BARRACKS, BarracksPrefab},
            {UnitID.MECH_BAY, MechBayPrefab},
            {UnitID.FACTORY, FactoryPrefab},
            {UnitID.TZEH, TzehPrefab},
            {UnitID.FIELD_LAB, FieldLabPrefab},
            {UnitID.HANGAR, HangarPrefab},

            {UnitID.BUILD_MECH_BAY, BuildMechBayPrefab},
            {UnitID.BUILD_BARRACKS, BuildBarracksPrefab},
            {UnitID.BUILD_FACTORY, BuildFactoryPrefab},
            {UnitID.BUILD_TZEH, BuildTzehPrefab},
            {UnitID.BUILD_FIELD_LAB, BuildFieldLabPrefab},
            {UnitID.BUILD_HANGAR, BuildHangarPrefab}
        };
    }

    public Dictionary<UnitID, Transform> BUILD_GHOSTS_BY_UIDS()
    {
        return new Dictionary<UnitID, Transform>{
            {UnitID.BARRACKS,   GhostBarracksPrefab},
            {UnitID.MECH_BAY,   GhostMechBayPrefab},
            {UnitID.FACTORY,    GhostFactoryPrefab},
            {UnitID.TZEH,       GhostTzehPrefab},
            {UnitID.FIELD_LAB,  GhostFieldLabPrefab},
            {UnitID.HANGAR,     GhostHangarPrefab}
        };
    }

    public Dictionary<UnitID, Vector2i> SIZE_BY_UID()
    {
        return new Dictionary<UnitID, Vector2i>{
            {UnitID.STONE, new Vector2i(1, 1)},
            {UnitID.ENERGY_POINT, new Vector2i(1, 1)},

            {UnitID.MARINE, new Vector2i(1, 1)},
            {UnitID.MERCENARY, new Vector2i(1, 1)},
            {UnitID.APC, new Vector2i(1, 1)},
            {UnitID.LIGHT_ASSAULT_BOT, new Vector2i(1, 1)},
            {UnitID.WARDOG, new Vector2i(2, 2)},
            {UnitID.TANK, new Vector2i(2, 2)},
            {UnitID.PROJECT_GRAVITY, new Vector2i(2, 2)},
            {UnitID.HELI, new Vector2i(1, 1)},
            {UnitID.QUADROCOPTER, new Vector2i(2, 2)},

            {UnitID.BARRACKS, new Vector2i(2, 2)},
            {UnitID.MECH_BAY, new Vector2i(3, 2)},
            {UnitID.FACTORY, new Vector2i(3, 2)},
            {UnitID.TZEH, new Vector2i(4, 2)},
            {UnitID.FIELD_LAB, new Vector2i(2, 2)},
            {UnitID.HANGAR, new Vector2i(2, 2)},

            {UnitID.BUILD_BARRACKS, new Vector2i(2, 2)},
            {UnitID.BUILD_MECH_BAY, new Vector2i(3, 2)},
            {UnitID.BUILD_FACTORY, new Vector2i(3, 2)},
            {UnitID.BUILD_TZEH, new Vector2i(4, 2)},
            {UnitID.BUILD_FIELD_LAB, new Vector2i(2, 2)},
            {UnitID.BUILD_HANGAR, new Vector2i(2, 2)}
        };
    }

    public Dictionary<UnitID, object[]> COMPONENTS_BY_UID()
    {
        return new Dictionary<UnitID, object[]>{
            {UnitID.STONE, new object[]{}},
            {UnitID.ENERGY_POINT, new object[]{
                new AbleToMine(4.0f, 0f)}},

            {UnitID.MARINE, new object[] {    
                new SimpleActiveObject(),
                new HP(15, 15, 0),
                new AbleToMoveByPath(1.75f, 720.0f),
                new AbleToAtackInstant(new AtackEventListenerDefault(), 1, 1 / 3f, 1f)}},
            {UnitID.MERCENARY, new object[] {    
                new SimpleActiveObject(),
                new HP(20, 20, 0),
                new AbleToMoveByPath(1.25f, 720.0f),
                new AbleToAtackInstant(new AtackEventListenerDefault(), 2, 1 / 2f, 2f)}},
            {UnitID.APC, new object[] {    
                new SimpleActiveObject(),
                new HP(40, 40, 0),
                new AbleToMoveByPath(2.0f, 720.0f),
                new AbleToAtackInstant(new AtackEventListenerDefault(), 2, 1 / 2f, 2f)}},
            {UnitID.LIGHT_ASSAULT_BOT, new object[] {    
                new SimpleActiveObject(),
                new HP(30, 30, 0),
                new AbleToMoveByPath(1.5f, 720.0f),
                new AbleToAtackInstant(new AtackEventListenerDefault(), 2, 1 / 0.625f, 8)}},
            {UnitID.WARDOG, new object[] {
                new SimpleActiveObject(),
                new HP(50, 50, 0),
                new AbleToMoveByPath(1.0f, 540.0f),
                new AbleToAtackInstant(new AtackEventListenerDefault(), 2, 1f, 7)}},
            {UnitID.TANK, new object[] {
                new SimpleActiveObject(),
                new HP(75, 75, 0),
                new AbleToMoveByPath(0.75f, 270.0f),
                new AbleToAtackInstant(new AtackEventListenerDefault(), 3, 1 / 0.5f, 16)}},
            {UnitID.PROJECT_GRAVITY, new object[] {
                new SimpleActiveObject(),
                new HP(75, 75, 0),
                new AbleToMoveByPath(0.25f, 270.0f),
                new AbleToAtackInstant(new AtackEventListenerDefault(), 4, 1 / 0.33f, 30)}},
            {UnitID.HELI, new object[] {
                new SimpleActiveObject(),
                new HP(75, 75, 0),
                new AbleToMoveByPath(0.75f, 270.0f),
                new AbleToAtackInstant(new AtackEventListenerDefault(), 1, 1 / 4.0f, 2)}},
            {UnitID.QUADROCOPTER, new object[] {
                new SimpleActiveObject(),
                new HP(75, 75, 0),
                new AbleToMoveByPath(0.75f, 270.0f),
                new AbleToAtackInstant(new AtackEventListenerDefault(), 2, 1 / 1.5f, 4)}},

            {UnitID.BARRACKS, new object[] {
                new SimpleActiveObject(),
                new HP(150, 150, 0),
                new AbleToBuild(ABLE_TO_BUILD_BY_UID()[UnitID.BARRACKS])}},
            {UnitID.MECH_BAY, new object[] {
                new SimpleActiveObject(),
                new HP(200, 200, 0),
                new AbleToBuild(ABLE_TO_BUILD_BY_UID()[UnitID.MECH_BAY])}},
            {UnitID.FACTORY, new object[] {
                new SimpleActiveObject(),
                new HP(150, 150, 0),
                new AbleToBuild(ABLE_TO_BUILD_BY_UID()[UnitID.FACTORY])}},
            {UnitID.TZEH, new object[] {
                new SimpleActiveObject(),
                new HP(200, 200, 0),
                new AbleToBuild(ABLE_TO_BUILD_BY_UID()[UnitID.TZEH])}},
            {UnitID.FIELD_LAB, new object[] {
                new SimpleActiveObject(),
                new HP(150, 150, 0),
                new AbleToBuild(ABLE_TO_BUILD_BY_UID()[UnitID.FIELD_LAB])}},
            {UnitID.HANGAR, new object[] {
                new SimpleActiveObject(),
                new HP(175, 175, 0),
                new AbleToBuild(ABLE_TO_BUILD_BY_UID()[UnitID.HANGAR])}},

            
            {UnitID.BUILD_BARRACKS, new object[] {
                new HP(150, 1, 0),
                new BuildStatePrefab()}},
            {UnitID.BUILD_MECH_BAY, new object[] {
                new HP(200, 1, 0),
                new BuildStatePrefab()}},
            {UnitID.BUILD_FACTORY, new object[] {
                new HP(225, 1, 0),
                new BuildStatePrefab()}},
            {UnitID.BUILD_TZEH, new object[] {
                new HP(250, 1, 0),
                new BuildStatePrefab()}},
            {UnitID.BUILD_FIELD_LAB, new object[] {
                new HP(200, 1, 0),
                new BuildStatePrefab()}},
            {UnitID.BUILD_HANGAR, new object[] {
                new HP(175, 1, 0),
                new BuildStatePrefab()}},
        };
    }

    public Dictionary<UnitID, ObjectToBuild> BUILD_PARAMS_BY_UID()
    {
        return new Dictionary<UnitID, ObjectToBuild>() {
            {UnitID.MARINE, new ObjectToBuild(UnitID.MARINE,                        4, 30, false)},
            {UnitID.MERCENARY, new ObjectToBuild(UnitID.MERCENARY,                  5, 40, false)},
            {UnitID.APC, new ObjectToBuild(UnitID.APC,                              7, 50, false)},
            {UnitID.LIGHT_ASSAULT_BOT, new ObjectToBuild(UnitID.LIGHT_ASSAULT_BOT,  7, 50, false)},
            {UnitID.WARDOG, new ObjectToBuild(UnitID.WARDOG,                        8, 75, false)},
            {UnitID.TANK, new ObjectToBuild(UnitID.TANK,                            12, 125, false)},
            {UnitID.PROJECT_GRAVITY, new ObjectToBuild(UnitID.PROJECT_GRAVITY,      20, 150, false)},
            {UnitID.HELI, new ObjectToBuild(UnitID.HELI,                            8, 50, false)},
            {UnitID.QUADROCOPTER, new ObjectToBuild(UnitID.QUADROCOPTER,            15, 100, false)},

            {UnitID.BARRACKS, new ObjectToBuild(UnitID.BARRACKS,    15, 150, true)},
            {UnitID.MECH_BAY, new ObjectToBuild(UnitID.MECH_BAY,    18, 150, true)},
            {UnitID.FACTORY, new ObjectToBuild(UnitID.FACTORY,      22, 225, true)},
            {UnitID.TZEH, new ObjectToBuild(UnitID.TANK,            30, 250, true)},
            {UnitID.FIELD_LAB, new ObjectToBuild(UnitID.FACTORY,    20, 200, true)},
            {UnitID.HANGAR, new ObjectToBuild(UnitID.TANK,          20, 175, true)}
        };
    }

    public Dictionary<UnitID, ObjectToBuild[]> ABLE_TO_BUILD_BY_UID()
    {
        return new Dictionary<UnitID, ObjectToBuild[]>{
            {UnitID.BARRACKS, new ObjectToBuild[] { 
                BUILD_PARAMS_BY_UID()[UnitID.MARINE], 
                BUILD_PARAMS_BY_UID()[UnitID.MERCENARY] }},
            {UnitID.MECH_BAY, new ObjectToBuild[] {}},
            {UnitID.FACTORY, new ObjectToBuild[] { 
                BUILD_PARAMS_BY_UID()[UnitID.APC],
                BUILD_PARAMS_BY_UID()[UnitID.LIGHT_ASSAULT_BOT], 
                BUILD_PARAMS_BY_UID()[UnitID.WARDOG],
                BUILD_PARAMS_BY_UID()[UnitID.TANK]}},
            {UnitID.TZEH, new ObjectToBuild[] { 
                BUILD_PARAMS_BY_UID()[UnitID.PROJECT_GRAVITY] }},
            {UnitID.FIELD_LAB, new ObjectToBuild[] {}},
            {UnitID.HANGAR, new ObjectToBuild[] { 
                BUILD_PARAMS_BY_UID()[UnitID.HELI],
                BUILD_PARAMS_BY_UID()[UnitID.QUADROCOPTER]}}
        };
    }

    public Dictionary<UnitID, UnitID[]> REQ_TO_BUILD_BY_UID()
    {
        return new Dictionary<UnitID, UnitID[]>{
            {UnitID.MARINE, new UnitID[] { 
                UnitID.BARRACKS}},
            {UnitID.MERCENARY, new UnitID[] { 
                UnitID.BARRACKS,
                UnitID.MECH_BAY}},
            {UnitID.APC, new UnitID[] { 
                UnitID.BARRACKS,
                UnitID.FACTORY}},
            {UnitID.LIGHT_ASSAULT_BOT, new UnitID[] { 
                UnitID.FACTORY}},
            {UnitID.WARDOG, new UnitID[] { 
                UnitID.MECH_BAY,
                UnitID.FACTORY}},
            {UnitID.TANK, new UnitID[] { 
                UnitID.FACTORY,
                UnitID.TZEH}},
            {UnitID.PROJECT_GRAVITY, new UnitID[] { 
                UnitID.BARRACKS,
                UnitID.MECH_BAY,
                UnitID.FACTORY,
                UnitID.TZEH}},
            {UnitID.HELI, new UnitID[] { 
                UnitID.MECH_BAY,
                UnitID.HANGAR}},
            {UnitID.QUADROCOPTER, new UnitID[] { 
                UnitID.FACTORY,
                UnitID.TZEH,
                UnitID.HANGAR}},

            {UnitID.BARRACKS, new UnitID[] {}},
            {UnitID.MECH_BAY, new UnitID[] {}},
            {UnitID.FACTORY, new UnitID[] {}},
            {UnitID.TZEH, new UnitID[] {}},
            {UnitID.FIELD_LAB, new UnitID[] {}},
            {UnitID.HANGAR, new UnitID[] {}}
        };
    }

    public UnitID[] GetBuildingUIDS()
    {
        return new UnitID[]{UnitID.BARRACKS, UnitID.MECH_BAY, UnitID.FACTORY, UnitID.TZEH, UnitID.FIELD_LAB, UnitID.HANGAR };
    }

    public float GetCost(UnitID id)
    {
        return BUILD_PARAMS_BY_UID()[id].BuildCost;
    }

    public Unit[] Units
    { get { return (Unit[])GameObject.FindObjectsOfType(typeof(Unit)); } }
 
    //===============================================================================================

    public GameObject CreateObject(UnitID uid, Vector2i position, Player player, ObjectToBuild buildParamsIfBuildingBuilt)
    {
        GameObject go = new GameObject();
        Unit unit = go.AddComponent<Unit>();
        go.AddComponent<SimpleSelectable>();

        GameObject imageGo = ((Transform)GameObject.Instantiate(PREFABS_BY_UIDS()[uid], Vector3.zero, Quaternion.identity)).gameObject;
        imageGo.transform.parent = go.transform;
        imageGo.transform.localPosition = Vector3.zero;
        imageGo.transform.localScale = new Vector3(1, 1, 1);
        imageGo.transform.Rotate(new Vector3(0, 0, 1), 180.0f);

        UnitInitParams parampampams = UNIT_PARAMS_BY_UID()[uid];
        unit.MustDownScale = parampampams.MustDownScale;
        go.name = parampampams.Name;
        unit.name = parampampams.Name;
        unit.Name = parampampams.Name;

        unit.IsTestObject = false;

        unit.Size = SIZE_BY_UID()[uid];
        //Vector2i pos = GameController.Map.GetFreePosition(size, position); // TODO WTF HERE
        unit.Position = new Position(go, unit.Size);
        unit.Position.BottomLeftOnMapMap = position;
        unit.Position.BottomLeftOnMapReal = position.ToVector2;
        GameController.Map.PutUnit(unit);

        unit.Init(player, uid);

        UnitFactoryGameObject.SetAll(unit, COMPONENTS_BY_UID()[uid]);

        if (buildParamsIfBuildingBuilt != null)
            unit.BuildState.Init(unit, buildParamsIfBuildingBuilt.BuildTime, UIDS_BY_BUILD_UIDS()[uid]);

        GameController.DistanceMap.OnAdd(unit);

        unit.InitComponentsFinally();

        return go;
    }



    public class UnitFactoryGameObject
    {

        public static void SetAll(Unit unit, object[] components)
        {
            for (int i = 0; i < components.Length; i++)
                Set(unit, components[i]);
        }

        private static void Set(Unit unit, object o)
        {
            if (o is AbleToAtackInstant)
                unit.AbleToAtack = (AbleToAtackInstant)o;
            else if (o is AbleToMoveByPath)
                unit.AbleToMove = (AbleToMoveByPath)o;
            else if (o is AbleToBuild)
                unit.AbleToBuild = (AbleToBuild)o;
            else if (o is HP)
                unit.HPController = (HP)o;
            else if (o is AActiveObject)
                unit.ActiveObject = (AActiveObject)o;
            else if (o is BuildStatePrefab)
                unit.BuildState = (BuildStatePrefab)o;
            else if (o is AbleToMine)
                unit.Mine = (AbleToMine)o;
            else
                Debug.LogError("FUUUUUU");
        }
    }

    public class UnitInitParams
    {
        public string Name;
        public bool MustDownScale = true;
        public UnitInitParams(string name) { this.Name = name; }
    }

    public BuildGhost CreateGhost(UnitID uid, Vector2i position)
    {
        GameObject go = new GameObject();
        go.AddComponent<BuildGhost>();

        GameObject imageGo = ((Transform)GameObject.Instantiate(BUILD_GHOSTS_BY_UIDS()[uid], Vector3.zero, Quaternion.identity)).gameObject;

        imageGo.transform.parent = go.transform;
        imageGo.transform.Rotate(new Vector3(0, 0, 1), 180.0f);


        Vector2i size = SIZE_BY_UID()[uid];
        go.transform.localScale = new Vector3(size.x, size.y, 1);
        go.GetComponent<BuildGhost>().Size = size;

        //Vector2i pos = GameController.Map.GetFreePosition(size, position); // TODO WTF HERE
        go.GetComponent<BuildGhost>().Position = new Position(go, size);
        go.GetComponent<BuildGhost>().Position.BottomLeftOnMapMap = position;
        go.GetComponent<BuildGhost>().Position.BottomLeftOnMapReal = position.ToVector2;
        
        return go.GetComponent<BuildGhost>();
    }
}
