﻿using UnityEngine;
using System.Collections;

public class MoveDirection {
    private Vector2i direction;

    private float distanceTraveled;
    private bool hasFinishedMove = false;

    public MoveDirection(Vector2i direction)
    {
        this.direction = direction;
        this.distanceTraveled = 0.0f;
    }

    public Vector2 ToVector2
    {
        get
        {
            return direction.ToVector2;
        }
    }

    public Vector2i ToVector2i
    {
        get
        {
            return direction;
        }
    }
    
    public float Distance
    {
        get
        {
            return distanceTraveled;
        }
        set
        {
            if (value > Magnitude)
            {
                distanceTraveled = Magnitude;
                OnReachDestination();
            }
            else
                distanceTraveled = value;
        }
    }

    public float LeftDistance
    {
        get
        {
            return Magnitude - Distance;
        }
    }

    public void OnReachDestination()
    {
        hasFinishedMove = true;
    }

    public float TargetAngleDeg
    {
        get
        {
            return Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        }
    }

    public bool HasStartedMove
    {
        get
        {
            return Distance != 0.0f;
        }
    }

    public bool HasFinishedMove
    {
        get
        {
            return hasFinishedMove;
        }
    }


    public float Magnitude
    {
        get
        {
            float len = Mathf.Abs(direction.x) + Mathf.Abs(direction.y);
            return (len > 1 ? Mathf.Sqrt(2) : len == 1 ? 1 : 0);
        }
    }

    public Vector2 PosAfterMove(float distance)
    {
        return direction.ToVector2 * distance / Magnitude;
    }

    public bool IsNull
    {
        get
        {
            return direction.IsZero();
        }
    }

    public static Vector2i GetDirection(Vector2 start, Vector2 target)
    {
        Vector2 diff = target - start;

        return new Vector2i(diff.x > 0 ? 1 : diff.x == 0 ? 0 : -1, diff.y > 0 ? 1 : diff.y == 0 ? 0 : -1);
    }

}
