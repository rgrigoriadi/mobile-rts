﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveSequence
{
    private List<MoveDirection> directions = new List<MoveDirection>();

    public void SetPath(List<Vector2i> path)
    {
        directions.Clear();

        for (int i = 0; i < path.Count; i++)
            directions.Add(new MoveDirection(path[i]));
    }

    public void Clear()
    {
        directions.Clear();
    }

    public bool IsEmpty
    {
        get
        {
            return directions.Count == 0;
        }
    }

    public MoveDirection CurrentMove 
    {
        get 
        {
            return IsEmpty ? null : directions[0];
        }
    }

    public void SwitchNextPoint()
    {
        directions.RemoveAt(0);
    }

    public int Length
    {
        get
        {
            return directions.Count;
        }
    }
}