﻿using UnityEngine;
using System.Collections;

public class AtackEventListenerChaseAndKill : AAtackEventListener 
{
    public AtackEventListenerChaseAndKill()
    {

    }

    public override void OnAtack(HP target)
    {
        //DONOTHING
    }

    public override void OnAtacked(AAbleToAtack atacker)
    {
        if (!Unit.ActiveObject.IsBusy())
            Unit.AbleToAtack.Atack(atacker.Unit.HPController, false);
    }

    public override void OnEnemyGetInAtackRange(Unit enemyInRange)
    {
        if (!Unit.ActiveObject.IsBusy())
            Unit.AbleToAtack.Atack(Unit.HPController, false);
    }
}
