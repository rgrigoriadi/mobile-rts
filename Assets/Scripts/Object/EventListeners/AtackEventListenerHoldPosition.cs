﻿using UnityEngine;
using System.Collections;

public class AtackEventListenerHoldPosition : AAtackEventListener {

    public AtackEventListenerHoldPosition()
    {
        //DONOTHING
    }

    public override void OnAtack(HP target)
    {
        //DONOTHING
    }

    public override void OnAtacked(AAbleToAtack atacker)
    {
        //DONOTHING
    }

    public override void OnEnemyGetInAtackRange(Unit enemyInRange)
    {
        if (!Unit.ActiveObject.IsBusy())
            Unit.AbleToAtack.Atack(enemyInRange.HPController, true);
    }
}
