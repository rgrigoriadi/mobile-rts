﻿using UnityEngine;
using System.Collections;

public class AtackEventListenerRunAway : AAtackEventListener {

    public AtackEventListenerRunAway()
    {

    }

    public override void OnAtack(HP target)
    {
        //DONOTHING
    }

    public override void OnAtacked(AAbleToAtack atacker)
    {
        throw new System.NotImplementedException();
    }

    public override void OnEnemyGetInAtackRange(Unit enemyInRange)
    {
        if (!Unit.ActiveObject.IsBusy())
            Unit.AbleToAtack.Atack(Unit.HPController, false);
    }
}
