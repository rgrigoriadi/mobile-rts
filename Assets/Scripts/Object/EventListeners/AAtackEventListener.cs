﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AAtackEventListener {
    public Unit Unit;
    private float lastPeriodicalUpdateTime = float.NaN;
    private const float updatePeriod = 0.5f;

    public const bool IS_DEBUG = false;

    protected List<Unit> enemyInRangeBuffer; // TODO REMOVE THIS SHIT
    protected List<Unit> meInEnemiesRange;

    public AAtackEventListener()
    {
        
    }

    public abstract void OnAtack(HP target);
    public abstract void OnAtacked(AAbleToAtack atacker);
    public abstract void OnEnemyGetInAtackRange(Unit enemyInRange);

    public void Update()
    {
        if (!Unit.ActiveObject.IsBusy())
            if (Unit.AbleToAtack != null)
            {
                Unit nearestEnemy = GameController.DistanceMap.GetNearestEnemyUnitInRangeOf(Unit, Unit.AbleToAtack.AtackRange());

                if (nearestEnemy != null)
                    OnEnemyGetInAtackRange(nearestEnemy);
            }
    }
}
