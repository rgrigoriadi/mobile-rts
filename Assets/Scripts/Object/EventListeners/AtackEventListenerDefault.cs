﻿using UnityEngine;
using System.Collections;

public class AtackEventListenerDefault : AAtackEventListener {

    public const bool IsDebugMode = false;

    public AtackEventListenerDefault()
        : base()
    {

    }

    public override void OnAtack(HP target)
    {
        //DONOTHING
    }

    public override void OnAtacked(AAbleToAtack atacker)
    {
        
        Unit.AbleToAtack.Atack(atacker.Unit.HPController, false);
    }

    public override void OnEnemyGetInAtackRange(Unit enemyInRange)
    {
        if (IsDebugMode)
            Debug.Log("Enemy " + enemyInRange.Name + " got in range of " + Unit.Name);

        //if (Unit.AbleToAtack == null)
        //    Debug.Log("NULL1");

        //if (enemyInRange.HPController == null)
        //    Debug.Log("NULL2");

        //if (enemyInRange == null)
        //    Debug.Log("NULL3");

        if (!Unit.ActiveObject.IsBusy())
            Unit.AbleToAtack.Atack(enemyInRange.HPController, true);
    }
}
