﻿using UnityEngine;
using System.Collections;

public abstract class AGUIForUnitLocalHandler : MonoBehaviour {

    public Unit Unit
    {
        get
        {
            return GetComponent<Unit>();
        }
    }

    public static Vector3 GetUnitOnScreenTop(Unit unit)
    {
        Vector3 pos = new Vector3(unit.transform.position.x, unit.transform.position.y, unit.transform.GetChild(0).GetComponent<MeshRenderer>().bounds.max.z);
        Vector3 onScreen = GameController.Camera.camera.WorldToScreenPoint(pos);
        return GUIGlobal.native2guiPos(onScreen);
    }

	// Use this for initialization
	void Start () {
	
	}

	void Update () {
        relocate();
	}

    public abstract void relocate();
}
