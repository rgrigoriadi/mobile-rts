﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIForUnitLocalHandler : AGUIForUnitLocalHandler
{
    IndicatorHandler indicators = new IndicatorHandler();
    ButtonsHandler buttons = new ButtonsHandler();
    GameObject selectionCircle;

    bool isActive = false;

    public void ShowGUI(bool willShowButtons)
    {
        HideGUI();
        isActive = true;

        indicators.ShowIndicatorsFor(Unit);

        if (willShowButtons)
            buttons.ShowButtons(Unit);

        showSelectionCircle();

        relocate();
    }

    public void HideGUI()
    {
        isActive = false;
        indicators.HideIndicators();
        buttons.HideButtons();
        hideSelectionCircle();
    }

    private void showSelectionCircle()
    {
        hideSelectionCircle();

        selectionCircle = ((Transform)GameObject.Instantiate(GameController.GUI.SelectionCirclePrefab)).gameObject;
        selectionCircle.transform.parent = transform;
        selectionCircle.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

        float targetRadius = Unit.Size.x / 0.7f /* sqrt(2) / 2 */ * 1.1f;

        float scaleCoef = targetRadius / selectionCircle.GetComponent<MeshRenderer>().bounds.size.x;

        Vector3 scale = selectionCircle.transform.localScale * scaleCoef;

        selectionCircle.transform.localScale = new Vector3(scale.x, scale.y, selectionCircle.transform.localScale.z);
    }

    private void hideSelectionCircle()
    {
        if (selectionCircle != null)
            GameObject.Destroy(selectionCircle);

        selectionCircle = null;
    }

    public override void relocate()
    {
        if (isActive)
        {
            indicators.Relocate(Unit);
            buttons.Relocate(Unit);
        }
    }

    private class IndicatorHandler
    {
        List<GameObject> activeIndicators = new List<GameObject>();

        //GameObject hpIndicator;
        //GameObject buildIndicator;
        //GameObject selfBuildIndicator;
        //GameObject captureIndicator;

        public void ShowIndicatorsFor(Unit unit)
        {
            HideIndicators();

            HP hp = GetHPProgress(unit);
            AbleToBuild builder = GetBuildProgress(unit);
            BuildStatePrefab selfBuilder = GetSelfBuildProgress(unit);
            AbleToMine mine = GetCaptureProgress(unit);

            if (hp != null)
                activeIndicators.Add(createIndicator(hp, GameController.GUI.HPProgressBarTex));

            if (builder != null)
                activeIndicators.Add(createIndicator(builder, GameController.GUI.BuildProgressBarTex));

            if (selfBuilder != null)
                activeIndicators.Add(createIndicator(selfBuilder, GameController.GUI.SelfBuildProgressTex));

            if (mine != null)
            {
                //Debug.Log("Showing capture indicator");
                activeIndicators.Add(createIndicator(mine, GameController.GUI.CaptureProgressTex));
            }
                
        }

        public void HideIndicators()
        {
            while (activeIndicators.Count > 0)
            {
                GameObject.Destroy(activeIndicators[0]);
                activeIndicators.RemoveAt(0);
            }
        }

        public void Relocate(Unit unit)
        {
            float padding = GUIGlobal.native2guiSize(5.0f);

            float coordX = GetUnitOnScreenTop(unit).x;
            float previousYTop = GetUnitOnScreenTop(unit).y;

            foreach (GameObject go in activeIndicators)
                previousYTop = showIndicator(go, coordX, previousYTop, padding);
        }

        private float showIndicator(GameObject go, float coordX, float prevMaxY, float padding)
        {
            GameObject[] gos = new GameObject[1];
            gos[0] = go;
            return showIndicators(gos, coordX, prevMaxY, padding);
        }

        private float showIndicators(GameObject[] gos, float coordX, float prevMaxY, float padding)
        {
            if (gos == null)
                return prevMaxY;

            for (int i = gos.Length - 1; i >= 0; i--)
                if (gos[i].GetComponent<ProgressBarIndicator>().WillShow())
                {
                    if (!gos[i].activeSelf)
                        gos[i].SetActive(true);

                    float halfSize = gos[i].transform.GetChild(0).GetComponent<MeshRenderer>().bounds.size.y / 2.0f; //GUIElement.GetSize(gos[i]).y / 2.0f;

                    Vector3 screenPos = new Vector3(coordX, prevMaxY + padding + halfSize, gos[i].transform.position.z);

                    gos[i].transform.position = screenPos;

                    prevMaxY = gos[i].transform.position.y + halfSize;
                }
                else if (gos[i].activeSelf)
                    gos[i].SetActive(false);

            return prevMaxY;
        }

        public AbleToBuild GetBuildProgress(Unit unit)
        {
            return unit.AbleToBuild;
        }

        public HP GetHPProgress(Unit unit)
        {
            return unit.HPController;
        }

        public BuildStatePrefab GetSelfBuildProgress(Unit unit)
        {
            return unit.BuildState;
        }

        public AbleToMine GetCaptureProgress(Unit unit)
        {
            return unit.Mine;
        }

        private GameObject createIndicator(ProgressBarIndicator.IProgressBarable dataProvider, Texture lineTex)
        {
            GameObject go = ((Transform) GameObject.Instantiate(GameController.GUI.ProgressBarPrefab)).gameObject;
            go.transform.parent = GameController.GUI.transform;

            ProgressBarIndicator bar = go.GetComponent<ProgressBarIndicator>();
            bar.Init(dataProvider, lineTex);
            return go;
        }

        private GameObject createIndicator(Transform prefab)
        {
             Transform trans = (Transform)GameObject.Instantiate(prefab);
             
             return trans.gameObject;
        }

    }

    private class ButtonsHandler
    {
        private GameObject[] shownButtons;

        public void ShowButtons(Unit unit)
        {
            InitButtons(UnitTaskButtonFactory.Instance.CreateActionButtonsForUnit(unit));
        }

        private void InitButtons(GameObject[] buttons)
        {
            HideButtons();

            float padding = 0;

            shownButtons = buttons;

            for (int i = 0; i < shownButtons.Length; i++)
            {   
                shownButtons[i].transform.parent = GameController.GUI.transform;

                Vector2 size = GUIGlobal.gui2nativeSize(shownButtons[i].GetComponent<GUIScaleScript>().GetSize());

                Vector2 pos = GUIGlobal.native2guiPos(new Vector2(
                    padding + size.x / 2.0f + i * (padding + size.x),
                    padding + size.y / 2.0f));

                shownButtons[i].transform.localPosition = new Vector3(
                    pos.x,
                    pos.y,
                    5.0f);

                //Debug.Log(
                //    "size " + shownButtons[i].GetComponent<GUIScaleScript>().GetSize() + " -> "
                //    + GUIGlobal.gui2nativeSize(shownButtons[i].GetComponent<GUIScaleScript>().GetSize())
                //    + " pos " + new Vector2(
                //    padding + size.x / 2.0f + i * (padding + size.x),
                //    padding + size.y / 2.0f) + " -> " + pos );
            }
        }

        public void HideButtons()
        {
            if (shownButtons != null)
                for (int i = 0; i < shownButtons.Length; i++)
                    GameObject.Destroy(shownButtons[i]);
        }

        public void Relocate(Unit unit)
        {

        }

    }
}
