﻿using UnityEngine;
using System.Collections;

public abstract class AMUIButton : MonoBehaviour {
    public abstract void OnPress();
}
