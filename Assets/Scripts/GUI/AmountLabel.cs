﻿using UnityEngine;
using System.Collections;

public class AmountLabel : MonoBehaviour {

    private IAmmountLabelable dataProvider;
    private Rect pos;
    private string prefix;

    public void Init(IAmmountLabelable dataProvider, string prefix, Rect pos)
    {
        this.dataProvider = dataProvider;
        this.pos = pos;
        this.prefix = prefix;
    }

    public void OnGUI()
    {
        setValue(dataProvider.GetValue());
    }

    private void setValue(int value)
    {
        GUI.Box(pos, prefix + value);
    }

    public interface IAmmountLabelable 
    {
        int GetValue();
    }
}
