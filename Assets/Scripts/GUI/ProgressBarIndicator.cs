﻿using UnityEngine;
using System.Collections;

public class ProgressBarIndicator : MonoBehaviour {

    private IProgressBarable dataProvider;

    public void Init(IProgressBarable dataProvider, Texture lineTex)
    {
        this.dataProvider = dataProvider;
        transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material.mainTexture = lineTex;
    }

    public bool WillShow()
    {
        return dataProvider.WillShow();
    }

    public void Update()
    {
        setProgress(dataProvider.GetProgress());
    }

    private void setProgress(float progress)
    {
        progress = progress > 1.0f ? 1.0f : progress < 0.0f ? 0.0f : progress;

        transform.GetChild(0).GetChild(0).localScale = new Vector3(progress, 1.0f, transform.localScale.z);
    }

    public interface IProgressBarable
    {
        float GetProgress();
        bool WillShow();
    }

}
