﻿using UnityEngine;
using System.Collections;

public class GUIScaleScript : MonoBehaviour {

    Vector3 defScale;

    void Start()
    {
        defScale = transform.localScale;

        Vector3 s = defScale;
        float scaleFactor = GUIGlobal.getScaleFactor();
        transform.localScale = new Vector3(s.x * scaleFactor, s.y * scaleFactor, s.z);
    }

	void Update () {

	}

    public Vector2 GetSize()
    {
        Vector3 s = GetComponent<MeshRenderer>().bounds.size;
        Vector3 sc = new Vector3(1, 1, 1);//transform.localScale;
        return new Vector2(s.x / sc.x, s.y / sc.y);
    }
}
