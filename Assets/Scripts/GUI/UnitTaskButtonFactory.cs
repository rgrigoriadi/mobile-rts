﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitTaskButtonFactory : MonoBehaviour {

    public Transform CancelSelectionButton;
    public Transform ActionButtonPrefab;    

    public Texture BuildMarineButtonTex;
    public Texture BuildMercenaryButtonTex;
    public Texture BuildAPCButtonTex;
    public Texture BuildLightAssaultBotButtonTex;
    public Texture BuildWardogButtonTex;
    public Texture BuildTankButtonTex;
    public Texture BuildProjectGravityButtonTex;
    public Texture BuildHeliButtonTex;
    public Texture BuildQuadrocopterButtonTex;
    //==   Texture
    public Texture BuildBarracksButtonTex;
    public Texture BuildMechBayButtonTex;
    public Texture BuildFactoryButtonTex;
    public Texture BuildTzehButtonTex;
    public Texture BuildFieldLabButtonTex;
    public Texture BuildHangarButtonTex;

    public Dictionary<UnitFactory.UnitID, Texture> BUILD_BUTTON_IMAGE_BY_UID()
    {
        return new Dictionary<UnitFactory.UnitID, Texture>()
        {
            {UnitFactory.UnitID.MARINE,             BuildMarineButtonTex},
            {UnitFactory.UnitID.MERCENARY,          BuildMercenaryButtonTex},
            {UnitFactory.UnitID.APC,                BuildAPCButtonTex},
            {UnitFactory.UnitID.LIGHT_ASSAULT_BOT,  BuildLightAssaultBotButtonTex},
            {UnitFactory.UnitID.WARDOG,             BuildWardogButtonTex},
            {UnitFactory.UnitID.TANK,               BuildTankButtonTex},
            {UnitFactory.UnitID.PROJECT_GRAVITY,    BuildProjectGravityButtonTex},
            {UnitFactory.UnitID.HELI,               BuildHeliButtonTex},
            {UnitFactory.UnitID.QUADROCOPTER,       BuildQuadrocopterButtonTex},
             
            {UnitFactory.UnitID.MECH_BAY,           BuildMechBayButtonTex},
            {UnitFactory.UnitID.BARRACKS,           BuildBarracksButtonTex},
            {UnitFactory.UnitID.FACTORY,            BuildFactoryButtonTex},
            {UnitFactory.UnitID.TZEH,               BuildTzehButtonTex},
            {UnitFactory.UnitID.FIELD_LAB,          BuildFieldLabButtonTex},
            {UnitFactory.UnitID.HANGAR,             BuildHangarButtonTex}
        };
    }

    private static UnitTaskButtonFactory instance;
    public static UnitTaskButtonFactory Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
    }

    public GameObject[] CreateActionButtonsForUnits(List<Unit> units)
    {
        List<GameObject> buttons = new List<GameObject>();

        //buttons.Add(CancelSelectionButton);

        //bool hasAddedMove = false;
        //bool hasAddedAtack = false;
        //for (int i = 0; i < units.Count; i++)
        //{
        //    if (!hasAddedMove)
        //    {
        //        if (units[i].AbleToMove != null)
        //        {
        //            buttons.Add(InstantiateButton(MoveButton, units[i]));
        //            hasAddedMove = true;
        //        }
        //    }

        //    if (!hasAddedAtack)
        //    {
        //        if (units[i].AbleToAtack != null)
        //        {
        //            buttons.Add(InstantiateButton(AtackButton, units[i]));
        //            hasAddedAtack = true;
        //        }
        //    }
        //}

        if (units.Count == 1) {
            AbleToBuild ableToBuild = units[0].AbleToBuild;

            if (ableToBuild != null)
            {
                buttons.AddRange(CreateBuildButtons(ableToBuild));
            }
        }

        return buttons.ToArray();
    }

    public GameObject[] CreateActionButtonsForUnit(Unit unit)
    {
        List<Unit> units = new List<Unit>();
        units.Add(unit);
        return CreateActionButtonsForUnits(units);
    }

    //public GameObject[] GetBuildBuildingsButtons()
    //{
            //TODO add ability to build buildings
    //}

    public GameObject[] CreateBuildButtons(AbleToBuild unit)
    {
        ObjectToBuild[] objects = unit.GetObjectsCanBuild();
        GameObject[] ret = new GameObject[objects.Length];

        for (int i = 0; i < ret.Length; i++)
        {
            //if (unit.Unit == null)
            //    Debug.LogError("OLOLO");
            ret[i] = CreateBuildButton(objects[i].ResultUID, unit.Unit);//button.gameObject;
        }
        return ret;
    }

    public GameObject CreateBuildButton(UnitFactory.UnitID id, Unit sender)
    {
        Transform button = (Transform)GameObject.Instantiate(ActionButtonPrefab);
        button.gameObject.AddComponent<MUIBuildUnitButton>();
        button.gameObject.GetComponent<MUIBuildUnitButton>().Init(id, sender);

        button.transform.GetChild(0).GetComponent<MeshRenderer>().material.mainTexture = BUILD_BUTTON_IMAGE_BY_UID()[id];
        return button.gameObject;
    }

    private GameObject[] CreateBuildBuildingsButtons(Player sender)
    {
        UnitFactory.UnitID[] buildings = GameController.UnitController.GetBuildingUIDS();

        GameObject[] ret = new GameObject[buildings.Length];

        for (int i = 0; i < buildings.Length; i++)
        {
            UnitFactory.UnitID id = buildings[i];
            Transform button = (Transform)GameObject.Instantiate(ActionButtonPrefab);
            button.gameObject.AddComponent<MUIBuildBuildingButton>().Init(id, sender);
            button.transform.GetChild(0).GetComponent<MeshRenderer>().material.mainTexture = BUILD_BUTTON_IMAGE_BY_UID()[id];
            ret[i] = button.gameObject;
        }
        return ret;
    }

    public GameObject[] ShowBuildBuildingsButtons(Player sender)
    {
        GameObject[] buttons = CreateBuildBuildingsButtons(sender);

        float padding = 0;

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].transform.parent = GameController.GUI.transform;

            Vector2 size = GUIGlobal.gui2nativeSize(buttons[i].GetComponent<GUIScaleScript>().GetSize());

            Vector2 pos = GUIGlobal.native2guiPos(new Vector2(
                padding + size.x / 2.0f + i * (padding + size.x),
                padding + size.y / 2.0f));

            buttons[i].transform.localPosition = new Vector3(
                pos.x,
                pos.y,
                5.0f);

            //Debug.Log(
            //    "size " + shownButtons[i].GetComponent<GUIScaleScript>().GetSize() + " -> "
            //    + GUIGlobal.gui2nativeSize(shownButtons[i].GetComponent<GUIScaleScript>().GetSize())
            //    + " pos " + new Vector2(
            //    padding + size.x / 2.0f + i * (padding + size.x),
            //    padding + size.y / 2.0f) + " -> " + pos );
        }

        return buttons;
    }
}
