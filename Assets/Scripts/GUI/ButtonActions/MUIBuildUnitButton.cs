﻿using UnityEngine;
using System.Collections.Generic;

public class MUIBuildUnitButton : AMUIButton {

    UnitFactory.UnitID objectToBuild;
    Unit builder;

    public void Init(UnitFactory.UnitID objToBuild, Unit builder)
    {
        this.objectToBuild = objToBuild;
        this.builder = builder;
    }

    public override void OnPress()
    {
        //Debug.Log("Laa");
        //return; 
        // Check money
        float cost = GameController.UnitController.GetCost(objectToBuild);
        float res = GameController.Diplomacy.CurrentPlayer.GetResources();
        if (cost > res)
            Debug.LogError("Not enough money for " + objectToBuild + " (need " + cost + ", have " + res + ")");
        else
        {
            // Check required buildings
            UnitFactory.UnitID[] reqIds = GameController.UnitController.REQ_TO_BUILD_BY_UID()[objectToBuild];

            List<UnitFactory.UnitID> stillRequiredList = new List<UnitFactory.UnitID>();

            if (builder == null)
                Debug.Log("1");
            if (builder.Player == null)
                Debug.Log("2");

            foreach (UnitFactory.UnitID id in reqIds)
                if (!builder.Player.HasUnit(id))
                    stillRequiredList.Add(id);

            if (stillRequiredList.Count == 0)
            {
                // TODO substract energy
                // Build
                builder.AbleToBuild.Build(objectToBuild);
            }
            else
            {
                string str = "Cannot build " + objectToBuild + ", need " + stillRequiredList[0];
                for (int i = 1; i < stillRequiredList.Count; i++)
                    str += ", " + stillRequiredList[i];
                Debug.LogError(str);
            }
        }
    }
}
