﻿using UnityEngine;
using System.Collections;

public class MUIAcceptBuildButton : AMUIButton
{
    public override void OnPress()
    {
        ((InputPickBuildPositionHandler)GameController.Input.CurrentHandler).Accept();
    }
}
