﻿using UnityEngine;
using System.Collections;

public class MUIShowBuildingMenuButton : AMUIButton {
    public override void OnPress()
    {
        GameController.GUI.HideBuildButton();
        GameController.GUI.ShowBuildingMenu();
    }
}
