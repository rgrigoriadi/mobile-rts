﻿using UnityEngine;
using System.Collections;

public class MUICancelBuildPosPickButton : AMUIButton
{
    public override void OnPress()
    {
        ((InputPickBuildPositionHandler)GameController.Input.CurrentHandler).Cancel();
    }
}
