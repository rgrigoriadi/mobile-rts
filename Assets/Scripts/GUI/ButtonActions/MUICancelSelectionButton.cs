﻿using UnityEngine;
using System.Collections;

public class MUICancelSelectionButton : AMUIButton {
    public override void OnPress()
    {
        GameController.Input.OnCancelButtonPress();
    }
}
