﻿using UnityEngine;
using System.Collections.Generic;

public class MUIBuildBuildingButton : AMUIButton
{

    UnitFactory.UnitID objectToBuild;
    Player sender;

    public void Init(UnitFactory.UnitID objToBuild, Player sender)
    {
        this.objectToBuild = objToBuild;
        this.sender = sender;
    }

    public override void OnPress()
    {
        // Check money
        // Show ghost and so on

		RaycastHit hit;
		RaycastController.RaycastZeroLevel(RaycastController.CursorToRay (new Vector2 (GUIGlobal.getResolution ().x / 2.0f, GUIGlobal.getResolution ().y / 2.0f)), out hit);
		Vector2 posOnMap = hit.point;
		Vector2 pos4ghost = GameController.Map.WorldToMap (posOnMap);

        Vector2i size = GameController.UnitController.SIZE_BY_UID()[objectToBuild];
        Vector2i resultPos = GameController.Map.GetFreePositionCenterCloseTo(pos4ghost, size);

        GameController.Input.AddLowestChild(
            new InputPickBuildPositionHandler(
                GameController.Input.CurrentHandler, 
                objectToBuild,
                GameController.Diplomacy.CurrentPlayer,
                GameController.UnitController.CreateGhost(
                    objectToBuild,
                    resultPos)));

        GameController.GUI.HideBuildingMenu();
        GameController.GUI.ShowBuildPosPickButtons();

        //throw new System.NotImplementedException();

        //float cost = GameController.UnitController.GetCost(objectToBuild);
        //float res = GameController.Diplomacy.CurrentPlayer.GetResources();
        //if (cost > res)
        //    Debug.LogError("Not enough money for " + objectToBuild + " (" + cost + ", " + res + ")");
        //else
        //{
        //    // Check required buildings
        //    UnitFactory.UnitID[] reqIds = GameController.UnitController.REQ_TO_BUILD_BY_UID()[objectToBuild];

        //    List<UnitFactory.UnitID> stillRequiredList = new List<UnitFactory.UnitID>();

        //    foreach (UnitFactory.UnitID id in reqIds)
        //        if (!builder.Player.HasUnit(id))
        //            stillRequiredList.Add(id);

        //    if (stillRequiredList.Count == 0)
        //    {
        //        // TODO substract energy
        //        // Build
        //        builder.AbleToBuild.Build(objectToBuild);
        //    }
        //    else
        //    {
        //        string str = "Cannot build " + objectToBuild + ", need " + stillRequiredList[0];
        //        for (int i = 1; i < stillRequiredList.Count; i++)
        //            str += ", " + stillRequiredList[i];
        //        Debug.LogError(str);
        //    }
        //}
    }
}
