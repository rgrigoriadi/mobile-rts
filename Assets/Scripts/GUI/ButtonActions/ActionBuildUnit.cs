﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActionBuildUnit : AActionBuild
{

    public override void Run()
    {
        // Check money
        float cost = GameController.UnitController.GetCost(ObjectToBuild);
        float res = GameController.Diplomacy.CurrentPlayer.GetResources();
        if (cost > res)
            Debug.LogError("Not enough money for " + ObjectToBuild + " (" + cost + ", " + res + ")");
        else
        {
            // Check required buildings
            UnitFactory.UnitID[] reqIds = GameController.UnitController.REQ_TO_BUILD_BY_UID()[ObjectToBuild];

            List<UnitFactory.UnitID> stillRequiredList = new List<UnitFactory.UnitID>();
        
            foreach (UnitFactory.UnitID id in reqIds)
                if (!Sender.Player.HasUnit(id))
                    stillRequiredList.Add(id);

            if (stillRequiredList.Count == 0)
            {
                // Build
                AbleToBuild builder = Sender.AbleToBuild;
                builder.Build(ObjectToBuild);
            }
            else
            { 
                string str = "Cannot build " + ObjectToBuild + ", need " + stillRequiredList[0];
                for (int i = 1; i < stillRequiredList.Count; i++)
                    str += ", " + stillRequiredList[i];
                Debug.LogError(str);
            }
        }
    }
}
