﻿using UnityEngine;
using System.Collections;

public class MUIHideBuildingMenuButton : AMUIButton
{
    public override void OnPress()
    {
        GameController.GUI.HideBuildingMenu();
        GameController.GUI.ShowBuildButton();
    }
}
