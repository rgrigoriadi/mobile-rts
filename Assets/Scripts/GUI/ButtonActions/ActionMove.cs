﻿using UnityEngine;
using System.Collections;

public class ActionMove : AAction {
    public override void Run()
    {
        GameController.Input.AddLowestChild(new InputMoveTaskHandler(GameController.Input.CurrentHandler));
    }
}
