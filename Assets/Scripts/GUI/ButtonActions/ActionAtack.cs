﻿using UnityEngine;
using System.Collections;

public class ActionAtack : AAction {

    public override void Run()
    {
        GameController.Input.AddLowestChild(new InputAtackTaskHandler(GameController.Input.CurrentHandler));
    }
}
