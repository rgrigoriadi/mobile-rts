﻿using UnityEngine;
using System.Collections;

public abstract class AActionBuild : AAction {
    public UnitFactory.UnitID ObjectToBuild;
}
