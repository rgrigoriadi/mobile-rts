﻿using UnityEngine;
using System.Collections;

public abstract class AAction : MonoBehaviour {

    public Unit Sender;

    public abstract void Run();
    
}
