﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIGlobal : MonoBehaviour{

    public Transform SelectionCirclePrefab;
    
    public  Transform  CancelButtonPrefab;
    private GameObject CancelButton;
    public  Transform  ShowBuildingMenuButtonPrefab;
    private GameObject ShowBuildingMenuButton;
    public  Transform  HideBuildingMenuButtonPrefab;
    private GameObject HideBuildingMenuButton;
    private GameObject[] BuildButtons;

    public Transform AcceptBuildButtonPrefab;
    public Transform CancelBuildPosPickButtonPrefab;
    private GameObject AcceptBuildButton;
    private GameObject CancelBuildPosPickButton;

    public Transform ResourceLabelPrefab;
    public Transform ProgressBarPrefab;
    public Texture BuildProgressBarTex;
    public Texture HPProgressBarTex;
    public Texture SelfBuildProgressTex;
    public Texture CaptureProgressTex;

    public const float SM_IN_INCH = 2.54f;
    public const float DEFAULT_DPI = 81.67f;
    public const float DEFAULT_HEIGHT_PX = 500.0f;
    public const float DEFAULT_HEIGHT_SM = DEFAULT_HEIGHT_PX * SM_IN_INCH / DEFAULT_DPI;


    public static Vector2 getResolution()
    {
        return new Vector2(Screen.width, Screen.height);
    }

    public static Vector2 native2guiPos(Vector2 native)
    {
        return new Vector2(
            (native.x - getResolution().x / 2.0f) / getResolution().y,
            (native.y - (getResolution().y / 2.0f)) / getResolution().y);
    }

    public static Vector2 gui2nativePos(Vector2 gui)
    {
        return (gui + new Vector2(0.5f, 0.5f)) * getResolution().y;
    }

    public static float native2guiSize(float native)
    {
        return native / getResolution().y;
    }

    public static float gui2nativeSize(float gui)
    {
        return gui * getResolution().y;
    }

    public static Vector2 native2guiSize(Vector2 native)
    {
        return native / getResolution().y;
    }

    public static Vector2 gui2nativeSize(Vector2 gui)
    {
        return gui * getResolution().y;
    }

    public static float getDPI()
    {
        return Screen.dpi == 0 ? DEFAULT_DPI : Screen.dpi;
    }

    public static float getScreenHeightSM()
    {
        return getResolution().y * SM_IN_INCH / getDPI();
    }

    public static float getScaleFactor()
    {
        return Mathf.Sqrt(getScreenHeightSM() / DEFAULT_HEIGHT_SM);
    }

    //==

    public void ShowCancelButton()
    {
        if (CancelButton == null)
        {
            CancelButton = ((Transform)GameObject.Instantiate(CancelButtonPrefab)).gameObject;
            CancelButton.transform.parent = GameController.GUI.transform;

            CancelButton.AddComponent<MUICancelSelectionButton>();

            float size = CancelButton.GetComponent<MeshRenderer>().bounds.size.x;
            Vector2 pos = native2guiPos(new Vector2(0.6f * gui2nativeSize(size), getResolution().y / 2.0f));
            CancelButton.transform.position = new Vector3(pos.x, pos.y, CancelButton.transform.position.z);
        }
    }

    public void HideCancelButton()
    {
        GameObject.Destroy(CancelButton);
        CancelButton = null;
    }

    public void ShowBuildButton()
    {
        if (ShowBuildingMenuButton == null)
        {
            ShowBuildingMenuButton = ((Transform)GameObject.Instantiate(ShowBuildingMenuButtonPrefab)).gameObject;
            ShowBuildingMenuButton.transform.parent = GameController.GUI.transform;

            ShowBuildingMenuButton.AddComponent<MUIShowBuildingMenuButton>();

            float size = ShowBuildingMenuButton.GetComponent<MeshRenderer>().bounds.size.x;
            Vector2 pos = native2guiPos(new Vector2(0.6f * gui2nativeSize(size), getResolution().y / 2.0f));
            ShowBuildingMenuButton.transform.position = new Vector3(pos.x, pos.y, ShowBuildingMenuButton.transform.position.z);
        }
    }

    public void HideBuildButton()
    {
        GameObject.Destroy(ShowBuildingMenuButton);
        ShowBuildingMenuButton = null;
    }

    public void ShowBuildingMenu()
    {
        HideBuildingMenuButton = ((Transform)GameObject.Instantiate(HideBuildingMenuButtonPrefab)).gameObject;
        HideBuildingMenuButton.transform.parent = GameController.GUI.transform;

        HideBuildingMenuButton.AddComponent<MUIHideBuildingMenuButton>();

        float size = HideBuildingMenuButton.GetComponent<MeshRenderer>().bounds.size.x;
        Vector2 pos = native2guiPos(new Vector2(0.6f * gui2nativeSize(size), getResolution().y / 2.0f));
        HideBuildingMenuButton.transform.position = new Vector3(pos.x, pos.y, HideBuildingMenuButton.transform.position.z);

        BuildButtons =  UnitTaskButtonFactory.Instance.ShowBuildBuildingsButtons(GameController.Diplomacy.CurrentPlayer);
    }

    public void HideBuildingMenu()
    {
        if (HideBuildingMenuButton != null)
            GameObject.Destroy(HideBuildingMenuButton);

        if (BuildButtons != null)
            foreach (GameObject go in BuildButtons)
                GameObject.Destroy(go);
    }

    public void ShowBuildPosPickButtons()
    {
        CancelBuildPosPickButton = ((Transform)GameObject.Instantiate(CancelBuildPosPickButtonPrefab)).gameObject;
        CancelBuildPosPickButton.transform.parent = GameController.GUI.transform;

        CancelBuildPosPickButton.AddComponent<MUICancelBuildPosPickButton>();

        float cancelSize = CancelBuildPosPickButton.GetComponent<MeshRenderer>().bounds.size.x;
        Vector2 pos = native2guiPos(new Vector2(0.6f * gui2nativeSize(cancelSize), getResolution().y / 2.0f));
        CancelBuildPosPickButton.transform.position = new Vector3(pos.x, pos.y, CancelBuildPosPickButton.transform.position.z);


        AcceptBuildButton = ((Transform)GameObject.Instantiate(AcceptBuildButtonPrefab)).gameObject;
        AcceptBuildButton.transform.parent = GameController.GUI.transform;

        AcceptBuildButton.AddComponent<MUIAcceptBuildButton>();

        float acceptSize = AcceptBuildButton.GetComponent<MeshRenderer>().bounds.size.x;
        pos = native2guiPos(new Vector2(1.2f * gui2nativeSize(acceptSize) + 0.6f * gui2nativeSize(cancelSize), getResolution().y / 2.0f));
        AcceptBuildButton.transform.position = new Vector3(pos.x, pos.y, AcceptBuildButton.transform.position.z);
    }

    public void HideBuildPosPickButtons()
    {
        GameObject.Destroy(CancelBuildPosPickButton);
        GameObject.Destroy(AcceptBuildButton);
    }

    public void ShowResourceLabel()
    {
        GameObject go = ((Transform) GameObject.Instantiate(ResourceLabelPrefab)).gameObject;

        float padding = 10;
        Vector2 size = new Vector2(100, 25);
        Vector2 pos = new Vector2(padding + size.x / 2.0f, /*getResolution().y - */(padding + size.y / 2.0f));
        
        go.GetComponent<AmountLabel>().Init(GameController.Diplomacy.CurrentPlayer.GetResourceController(), "Energy : ", new Rect(pos.x - size.x / 2.0f, pos.y - size.y / 2.0f, size.x, size.y));
    }

    public void OnPlayerChange(Player newPlayer) 
    {
        
    }

    public void OnSelectionChange()
    {
        if (GameController.SelectionController.GetSelectedUnits().Count > 0)
        {
            HideBuildButton();
            HideBuildingMenu();
            ShowCancelButton();
        }
        else
        {
            HideCancelButton();
            ShowBuildButton();
        }
    }

    public void OnCantSpendResourcesError()
    {

    }
}
