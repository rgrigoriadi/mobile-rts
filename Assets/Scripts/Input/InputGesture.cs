﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputGesture {

    private List<TouchPoint> gesture = new List<TouchPoint>();

    public class TouchPoint
    {
        public Vector2 Position;
        public float Time;

        public TouchPoint(Vector2 pos, float absoluteTime)
        {
            this.Position = pos;
            this.Time = absoluteTime;
        }
    }

    public void OnFirstTouch(Vector2 position)
    {
        gesture.Clear();
        OnTouch(position);
    }

    public void OnTouch(Vector2 position)
    {
        gesture.Add(new TouchPoint(position, Time.time));
    }

    public float TouchTime
    {
        get { return gesture[gesture.Count - 1].Time - gesture[0].Time; }
    }

    public Vector2 MaxDelta
    {
        get
        {
            Vector2 min = gesture[0].Position;
            Vector2 max = gesture[1].Position;

            foreach (TouchPoint touch in gesture)
            {
                Vector2 pos = touch.Position;

                if (pos.x < min.x)
                    min.x = pos.x;
                if (pos.y < min.y)
                    min.y = pos.y;

                if (pos.x > max.x)
                    max.x = pos.x;
                if (pos.y > max.y)
                    max.y = pos.y;
            }

            return max - min;
        }
    }

    public float MaxDeltaMagnitude {
        get { return MaxDelta.magnitude; }
    }

}
