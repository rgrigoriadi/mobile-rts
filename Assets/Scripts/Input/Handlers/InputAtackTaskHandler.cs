﻿using UnityEngine;
using System.Collections;

public class InputAtackTaskHandler : InputHandler
{
    private bool mustDebug = true;

    public InputAtackTaskHandler(InputHandler parent)
        : base(parent)
    {

    }

    public override void OnShortTapEnd(Vector2 position)
    {
        if (mustDebug)
            Debug.Log("Atack on touch start");

        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMapAndObject(RaycastController.CursorToRay(position), out hit))
        {
            if (hit.collider.gameObject.layer == RaycastController.LAYER_UNIT)
            {
                HP target = hit.collider.transform.parent.GetComponent<Unit>().HPController;

                if (mustDebug)
                    Debug.Log("Touch hit unit");

                if (target != null)
                {
                    for (int i = 0; i < GameController.SelectionController.GetSelectedUnits().Count; i++)
                    {
                        AAbleToAtack atacker = GameController.SelectionController.GetSelectedUnits().List[i].AbleToAtack;
                        if (atacker != null)
                        {
                            atacker.Atack(target, false);
                        }
                    }
                }
            }
            else if (hit.collider.gameObject.layer == RaycastController.LAYER_MAP)
            {
                Vector2 target = GameController.Map.WorldToMap(hit.point);

                if (mustDebug)
                    Debug.Log("Touch hit map");


                for (int i = 0; i < GameController.SelectionController.GetSelectedUnits().Count; i++)
                {
                    AAbleToAtack atacker = GameController.SelectionController.GetSelectedUnits().List[i].AbleToAtack;
                    if (atacker != null)
                    {
                        atacker.Atack(target);
                    }
                }
            }
        } else
        if (mustDebug)
            Debug.Log("Touch missed");

        Controller.RemoveLowestChild();
    }

    public override void OnPress(Vector2 position, bool isShort)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMapAndObject(RaycastController.CursorToRay(position), out hit))
            if (hit.collider.gameObject.layer == RaycastController.LAYER_MAP)
                if (!isShort)
                    Controller.AddLowestChild(new InputMapDragHandler(parentHandler, position));
    }
}
