﻿using UnityEngine;
using System.Collections;

public class InputIdleHandler : InputHandler
{
    public InputIdleHandler(InputHandler parent)
        : base(parent)
    {

    }

    public override void OnShortTapEnd(Vector2 position)
    {
        OnPress(position, true);
        OnLongPressEnd(position);
    }

    public override void OnPress(Vector2 position, bool isShort)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMapAndObject(RaycastController.CursorToRay(position), out hit))
        {
            //Debug.Log("Hit " + hit.collider.name + " layer " + hit.collider.gameObject.layer + " layer mask = " + RaycastController.GetMask(hit.collider.gameObject.layer));
            if (hit.collider.GetComponent<MapBack>() != null)
            {
                Controller.AddLowestChild(new InputMapDragHandler(this, position));
            }
            else if (hit.collider.transform.parent.GetComponent<SimpleSelectable>() != null)
            {
                if (hit.collider.transform.parent.GetComponent<Unit>().Player.isAllyOf(GameController.Diplomacy.CurrentPlayer))
                    Controller.AddLowestChild(new InputSelectingHandler(this, position));
                else
                    Debug.LogError("Cannot select enemy units.");
            }
        }
    }

    public override void OnLongPressEnd(Vector2 position)
    {
        if (childHandler != null) // Если детку добавили, а он лонгПресс вызвали до добавления детки
            childHandler.CTRLonLongPressEnd(position);
    }
}
