﻿using UnityEngine;
using System.Collections;

public class InputMapDragHandler : InputHandler {

    //public float previousTouchX;
    public Vector2 previousTouchPosition;

    public InputMapDragHandler(InputHandler parent, Vector2 position) : base(parent)
    {
        //previousTouchX = getIntersectionWithZeroLevel(position);

        previousTouchPosition = position;
        //OnPress(position);
    }

    public override void OnShortTapEnd(Vector2 position)
    {
        Controller.RemoveLowestChild();
        parentHandler.OnShortTapEnd(position); // WHY??
        ///OnPress(position, true);
        ///OnLongPressEnd(position);
    }


    public override void OnPress(Vector2 position, bool isShort)
    {
        if (isShort)
            return;

        float newX = getIntersectionWithZeroLevel(position);

        float oldX = getIntersectionWithZeroLevel(previousTouchPosition);

        if (!float.IsNaN(newX))
        {
            //Debug.Log("intersection with zero-level x = " + newX);

            GameController.Camera.X += -(newX - oldX);

            //previousTouchX = newX;
        }
        else
        {
            Debug.LogWarning("Intersection with zero level not found");
        }

        previousTouchPosition = position;
    }

    public override void OnLongPressEnd(Vector2 position)
    {
        Controller.RemoveLowestChild();
        GameController.Camera.RunInertialScroll(-(position.x - previousTouchPosition.x));// * Time.deltaTime);//10.0f);
        // (position.x - previousTouchPosition.x) > 0 ? -10.0f : (position.x - previousTouchPosition.x) < 0 ? 10.0f : 0.0f);
    }

    private float getIntersectionWithZeroLevel(Vector2 position)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastZeroLevel(RaycastController.CursorToRay(position), out hit))
        {
            //Debug.Log("Collisiont with " + hit.collider.gameObject.name);
            return hit.point.x;
        }
        else
            return float.NaN;
    }
}
