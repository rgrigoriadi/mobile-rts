﻿using UnityEngine;

public abstract class InputHandler 
{
    public readonly Vector2 NOT_SET = new Vector2(-1.0f, -1.0f);

    protected InputHandler childHandler = null;
    protected InputHandler parentHandler;

    public InputHandler(InputHandler parent)
    {
        this.parentHandler = parent;
    }

    public InputController Controller
    {
        get
        {
            return GameController.Input;
        }
    }

    public void CTRLonPress(Vector2 position, bool isShort)
    {
        if (childHandler != null)
            childHandler.CTRLonPress(position, isShort);
        else
        {
            //Debug.Log("OnPress " + this.GetType().Name);
            GameController.Camera.StopInertialScroll(); // Tap on not GUI will stop scroll
            OnPress(position, isShort);
        }
    }

    public void CTRLonLongPressEnd(Vector2 position)
    {
        if (childHandler != null)
            childHandler.CTRLonLongPressEnd(position);
        else
            OnLongPressEnd(position);
    }

    public void CTRLonShortTapEnd(Vector2 position)
    {
        if (childHandler != null)
            childHandler.CTRLonShortTapEnd(position);
        else
            OnShortTapEnd(position);
    }
    
    public virtual void OnPress(Vector2 position, bool isShort)
    {
        
    }

    public virtual void OnLongPressEnd(Vector2 position)
    {
    
    }

    public virtual void OnShortTapEnd(Vector2 position)
    {
    
    }

    public InputHandler LowestChild
    {
        get
        {
            return childHandler == null ? this : childHandler;
        }
    }

    public void SetChildHandler(InputHandler newHandler) 
	{
        RemoveChild();
        childHandler = newHandler;
    }

    public void RemoveChild()
    {
        if (childHandler != null)
        {
            childHandler.RemoveChild();
            childHandler.OnRemove();
            childHandler = null;
        }
    }

    public virtual void OnRemove() {
        
    }

    public void RemoveLowestChild()
    {
        if (childHandler != null)
            childHandler.RemoveLowestChild();
        else if (parentHandler != null)
            parentHandler.RemoveChild();
    }

    public void AddLowestChild(InputHandler child)
    {
        if (childHandler != null)
            childHandler.AddLowestChild(child);
        else
            SetChildHandler(child);
    }	
}
