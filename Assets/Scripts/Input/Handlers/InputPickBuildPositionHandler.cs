﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputPickBuildPositionHandler : InputHandler {

    private UnitFactory.UnitID objectToBuild;
    private BuildGhost ghost;
    private Player player;
    
    public InputPickBuildPositionHandler(InputHandler parent, UnitFactory.UnitID objectToBuild, Player player, BuildGhost ghost)
        : base(parent)
    {
        this.objectToBuild = objectToBuild;
        this.ghost = ghost;
        this.player = player;
    }

    public override void OnShortTapEnd(Vector2 position)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMapAndObject(RaycastController.CursorToRay(position), out hit))
        {
            if (hit.collider.gameObject.layer == RaycastController.LAYER_UNIT)
            {
                OnPressOccupied();
            }
            else if (hit.collider.gameObject.layer == RaycastController.LAYER_MAP)
            {
                // Draw ghost here
                putGhostTo(hit.point);
            }
        }
    }

    public override void OnPress(Vector2 position, bool isShort)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMapAndObject(RaycastController.CursorToRay(position), out hit))
        {
            if (hit.collider.gameObject.layer == RaycastController.LAYER_UNIT)
            {
                if (hit.collider.transform.parent.gameObject != ghost.gameObject)
                    OnPressOccupied();
                else
                {
                    GameController.Input.AddLowestChild(
                        new GhostMover(this, ghost));
                }
            }
            else if (hit.collider.gameObject.layer == RaycastController.LAYER_MAP)
            {
                if (!isShort)
                    Controller.AddLowestChild(new InputMapDragHandler(this, position));
            }
        }
    }
    
    private void OnPressOccupied()
    {
        Debug.LogError("Cannot place " + objectToBuild + " here");
    }

    public void putGhostTo(Vector2 pos)
    {
        Vector2 posOnMap = GameController.Map.WorldToMap(pos);

        Vector2i size = GameController.UnitController.SIZE_BY_UID()[objectToBuild];
        ghost.Position.BottomLeftOnMapMap = GameController.Map.GetFreePositionCenterCloseTo(posOnMap, size);
        ghost.Position.BottomLeftOnMapReal = GameController.Map.GetFreePositionCenterCloseTo(posOnMap, size).ToVector2;
    }

    public void Accept()
    {
        GameController.GUI.HideBuildPosPickButtons();
        GameController.GUI.ShowBuildingMenu();

        GameController.Input.RemoveLowestChild();
        RemoveGhost();
        
        GameController.UnitController.CreateObject(
            GameController.UnitController.BUILD_UIDS_BY_UIDS()[objectToBuild], 
            ghost.Position.BottomLeftOnMapMap, 
            player, 
            GameController.UnitController.BUILD_PARAMS_BY_UID()[objectToBuild]);
    }

    public void Cancel()
    {
        GameController.GUI.HideBuildPosPickButtons();
        GameController.GUI.ShowBuildingMenu();

        GameController.Input.RemoveLowestChild();
        RemoveGhost();
    }

    private void RemoveGhost()
    {
        GameObject.Destroy(ghost.gameObject);
    }

    private class GhostMover : InputHandler
    {
        private BuildGhost ghost;
        public GhostMover(InputHandler handler, BuildGhost ghost) : base(handler)
        {
            Debug.Log("Constructor in child");
            this.ghost = ghost;
        }

        public override void OnPress(Vector2 position, bool isShort)
        {
            //Debug.Log("OnPress in child");

            RaycastHit hit = new RaycastHit();
            if (RaycastController.RaycastMap(RaycastController.CursorToRay(position), out hit))
                ((InputPickBuildPositionHandler)parentHandler).putGhostTo(hit.point);
        }

        public override void OnLongPressEnd(Vector2 position)
        {
            Debug.Log("Long End in child");

            this.Controller.RemoveLowestChild(); // Урать это контроллер
        }
    }
}

