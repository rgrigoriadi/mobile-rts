﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputDefaultActionHandler : InputHandler {

    public InputDefaultActionHandler(InputHandler parent)
        : base(parent)
    {

    }

    public override void OnShortTapEnd(Vector2 position)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMapAndObject(RaycastController.CursorToRay(position), out hit))
        {
            if (hit.collider.gameObject.layer == RaycastController.LAYER_UNIT)
            {
                Unit target = hit.collider.transform.parent.GetComponent<Unit>();
                processUnitTap(target, position);
            }
            else if (hit.collider.gameObject.layer == RaycastController.LAYER_MAP)
            {
                processMapTap(hit, position);
            }
        }
    }

    private void processUnitTap(Unit target, Vector2 position)
    {
        

        if (target != null)
        {
            if (target.Player == GameController.Diplomacy.CurrentPlayer)
            {
                Controller.ReplaceLowestWithNew(new InputSelectingHandler(parentHandler, position));
                Controller.CurrentHandler.OnLongPressEnd(position);
                return;
            }
            else if (target.Player.isAllyOf(GameController.Diplomacy.CurrentPlayer) || target.Player.isNeutral())
                for (int i = 0; i < GameController.SelectionController.GetSelectedUnits().Count; i++)
                {
                    AAbleToMove mover = GameController.SelectionController.GetSelectedUnits().List[i].AbleToMove;
                    if (mover != null)
                        mover.Move(target);
                        
                }
            else if (target.Player.isEnemyOf(GameController.Diplomacy.CurrentPlayer))
            {
                HP targetToAtack = target.HPController;

                if (targetToAtack != null)
                {
                    for (int i = 0; i < GameController.SelectionController.GetSelectedUnits().Count; i++)
                    {
                        AAbleToAtack atacker = GameController.SelectionController.GetSelectedUnits().List[i].AbleToAtack;
                        if (atacker != null)
                        {
                            atacker.Atack(targetToAtack, false);
                        }
                    }
                }
            }
        }
    }

    private void processMapTap(RaycastHit hit, Vector2 position)
    {
        Vector2 target = GameController.Map.WorldToMap(hit.point);

        //Debug.Log("Tapped " + position + " target = " + target);

        if (GameController.Map.IsBusy(GameController.Map.MapCell(target)))
            processUnitTap(GameController.Map.GetUnitAt(GameController.Map.MapCell(target)), position);
        else
            for (int i = 0; i < GameController.SelectionController.GetSelectedUnits().Count; i++)
            {
                AAbleToMove mover = GameController.SelectionController.GetSelectedUnits().List[i].AbleToMove;
                if (mover != null)
                    mover.Move(target);
            }
    }

    public override void OnPress(Vector2 position, bool isShort)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMapAndObject(RaycastController.CursorToRay(position), out hit))
        {
            if (hit.collider.gameObject.layer == RaycastController.LAYER_UNIT)
            {
                Unit target = hit.collider.transform.parent.GetComponent<Unit>();

                if (target != null)
                {
                    if (target.Player == GameController.Diplomacy.CurrentPlayer)
                    {
                        Controller.ReplaceLowestWithNew(new InputSelectingHandler(parentHandler, position));
                        //Controller.CurrentHandler.OnLongPressEnd(position);
                        return;
                    }
                }
            }
            else if (hit.collider.gameObject.layer == RaycastController.LAYER_MAP)
            {
                if (!isShort)
                    Controller.AddLowestChild(new InputMapDragHandler(this, position));
            }
        }
    }
}
