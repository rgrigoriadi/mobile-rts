﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AInputShortActionHandler : InputHandler {

    public AInputShortActionHandler(InputHandler parent)
        : base(parent)
    {
    
    }

    public sealed override void OnShortTapEnd(Vector2 position)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMapAndObject(RaycastController.CursorToRay(position), out hit))
        {
            if (hit.collider.gameObject.layer == RaycastController.LAYER_UNIT)
            {
                Unit target = hit.collider.transform.parent.GetComponent<Unit>();
                if (target != null)
                {
                    if (target.Player == GameController.Diplomacy.CurrentPlayer)
                    {
                        if (allowReselect())
                        {
                            Controller.ReplaceLowestWithNew(new InputSelectingHandler(parentHandler, position));
                            Controller.CurrentHandler.OnLongPressEnd(position);
                        }
                        else PlayersUnitTapped(target);
                        return;
                    }
                    else if (target.Player.isAllyOf(GameController.Diplomacy.CurrentPlayer) || target.Player.isNeutral())
                        FriendlyPlayersUnitTapped(target);
                    else if (target.Player.isEnemyOf(GameController.Diplomacy.CurrentPlayer))
                    {
                        EnemyPlayersUnitTapped(target);
                    }
                }
                else Debug.LogError("WTF, Of UNIT layer, but no unit!");
            }
            else if (hit.collider.gameObject.layer == RaycastController.LAYER_MAP)
            {
                Vector2 target = GameController.Map.WorldToMap(hit.point);
                MapTapped(target);
            }
        }
    }

    public sealed override void OnPress(Vector2 position, bool isShort)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMapAndObject(RaycastController.CursorToRay(position), out hit))
        {
            if (hit.collider.gameObject.layer == RaycastController.LAYER_UNIT)
            {
                Unit target = hit.collider.transform.parent.GetComponent<Unit>();

                if (target != null)
                {
                    if (allowReselect())
                    {
                        Controller.ReplaceLowestWithNew(new InputSelectingHandler(parentHandler, position));
                        Controller.CurrentHandler.OnLongPressEnd(position);
                    }
                }
            }
            else if (hit.collider.gameObject.layer == RaycastController.LAYER_MAP)
            {
                if (!isShort)
                    if (allowScroll())
                    {
                        Controller.AddLowestChild(new InputMapDragHandler(this, position));
                        return;
                    }
                Debug.LogError("WTF Long tap long? WAT TO DO?");
            }
        }
    }

    protected abstract bool allowScroll();
    protected abstract bool allowReselect();

    protected abstract void PlayersUnitTapped(Unit unit);
    protected abstract void FriendlyPlayersUnitTapped(Unit unit);
    protected abstract void EnemyPlayersUnitTapped(Unit unit);
    protected abstract void MapTapped(Vector2 pos);
    
}
