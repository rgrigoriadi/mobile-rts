﻿using UnityEngine;
using System.Collections;

public class InputSelectingHandler : InputHandler {

    public InputSelectingHandler(InputHandler parent, Vector2 position)
        : base(parent)
    {

        //Debug.Log("[Selecting] constructor");

        GameController.SelectionController.ResetSelection();
        OnPress(position, false);

    }

    public override void OnShortTapEnd(Vector2 position)
    {
        //Debug.Log("[Selecting] tap -> press end");

        OnPress(position, true);
        OnLongPressEnd(position);
    }


    public override void OnPress(Vector2 position, bool isShort)
    {
        //Debug.Log("[Selecting] longPress");

        processObjects(position);
    }

    public override void OnLongPressEnd(Vector2 position)
    {
        //Debug.Log("[Selecting] longPressEnd");

        Controller.ReplaceLowestWithNew(new InputDefaultActionHandler(parentHandler));
    }

    private void processObjects(Vector2 position)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMapAndObject(GameController.Camera.camera.ScreenPointToRay(position), out hit))
        {
            if (hit.collider.transform.parent.GetComponent<SimpleSelectable>() != null)
            {
                hit.collider.transform.parent.GetComponent<SimpleSelectable>().Select();
            }
        }
    }
}
