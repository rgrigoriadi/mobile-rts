﻿using UnityEngine;
using System.Collections;

public class InputMoveTaskHandler : InputHandler {

    public InputMoveTaskHandler(InputHandler parent)
        : base(parent)
    {
        throw new System.NotImplementedException(
            "No more need in unit mover, default action handler does this, plus current version is old and need to be corrected");
    }

    public override void OnShortTapEnd(Vector2 position)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMapAndObject(RaycastController.CursorToRay(position), out hit))
        {
            if (hit.collider.gameObject.layer == RaycastController.LAYER_UNIT)
            {
                Unit target = hit.collider.transform.parent.GetComponent<Unit>();

                if (target != null)
                {
                    for (int i = 0; i < GameController.SelectionController.GetSelectedUnits().Count; i++)
                    {
                        AAbleToMove mover = GameController.SelectionController.GetSelectedUnits().List[i].AbleToMove;
                        if (mover != null)
                        {
                            //mover.Move(target);
                            throw new System.NotImplementedException();
                        }
                    }
                }
            }
            else if (hit.collider.gameObject.layer == RaycastController.LAYER_MAP)
            {
                Vector2 target = GameController.Map.WorldToMap(hit.point);
                
                    for (int i = 0; i < GameController.SelectionController.GetSelectedUnits().Count; i++)
                    {
                        AAbleToMove mover = GameController.SelectionController.GetSelectedUnits().List[i].AbleToMove;
                        if (mover != null)
                        {
                            mover.Move(target);
                        }
                }
            }
        }

        Controller.RemoveLowestChild();
    }

    public override void OnPress(Vector2 position, bool isShort)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMapAndObject(RaycastController.CursorToRay(position), out hit))
        {
            /*
            if (hit.collider.gameObject.layer == RaycastController.LAYER_UNIT)
            {
                Unit target = hit.collider.GetComponent<Unit>();

                if (target != null)
                {
                    for (int i = 0; i < GameController.SelectionController.GetSelectedUnits().Count; i++)
                    {
                        AAbleToMove mover = GameController.SelectionController.GetSelectedUnits().List[i].AbleToMove;
                        if (mover != null)
                        {
                            mover.Move(target);
                            Controller.RemoveLowestChild();
                        }
                    }
                }
            }
            else  */
            if (hit.collider.gameObject.layer == RaycastController.LAYER_MAP)
            {
                if (!isShort)
                    Controller.AddLowestChild(new InputMapDragHandler(this, position));
            }
        }
    }

    public override void OnLongPressEnd(Vector2 position)
    {

    }
}
