﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputController : TouchTargetedDelegate
{

    private InputHandler topHandler;

    private bool isGuiInput = false;
    private bool isLongPress = false;

    private Gesture gesture;

    public InputController()
    {
        gesture = new Gesture();
        ((TouchDispatcher)GameObject.FindObjectOfType(typeof(TouchDispatcher))).addTargetedDelegate(this, 10, false);
        topHandler = new InputIdleHandler(null);
    }

    public bool TouchBegan(Vector2 position, int fingerId)
    {
        if (RaycastController.IsGUIHit(position))
        {
            SetGUIInput();

            RaycastHit hit = new RaycastHit();
            if (RaycastController.RaycastGUI(position, out hit))
            {
                //Debug.Log(hit.collider.gameObject.name);
                AMUIButton button = hit.collider.gameObject.GetComponent<AMUIButton>();
                button.OnPress();
            }
        }
        else
        {
            isLongPress = false;
            gesture.Clear();
            TouchMoved(position, fingerId);
        }

        return true;
    }

    public void TouchMoved(Vector2 position, int fingerId)
    {
        if (!IsGUIInput())
        {
            gesture.AddTouch(position, Time.time);

            if (!isLongPress && !gesture.IsTapMap)
                isLongPress = true;
            OnPress(position, !isLongPress);
        }
    }

    public void TouchEnded(Vector2 position, int fingerId)
    {
        if (!IsGUIInput())
        {
            if (isLongPress)
                OnLongTouchEnded(position);
            else
                OnShortTapEnded(position);
        }
        else
            SetNormalInput();

    }

    public void TouchCanceled(Vector2 position, int fingerId)
    {
        TouchEnded(position, fingerId);
    }

    public void OnPress(Vector2 position, bool isShort)
    {
        topHandler.CTRLonPress(position, isShort);
    }

    public void OnShortTapEnded(Vector2 position)
    {
        topHandler.CTRLonShortTapEnd(position);
    }

    public void OnLongTouchEnded(Vector2 position)
    {
        topHandler.CTRLonLongPressEnd(position);
    }

    public InputHandler CurrentHandler
    {
        get
        {
            return topHandler.LowestChild;
        }
    }

    public void OnCancelButtonPress()
    {
        //if (currentHandler != null)
        //    if (!IsInputIgnored())
        //        currentHandler.OnCancelButtonPressed();

        GameController.SelectionController.ResetSelection();
        //RemoveAllHandlers();
    }

    public void RemoveLowestChild()
    {
        //Debug.Log("[InputHandler] Remove lowest child " + CurrentHandler.GetType().FullName);
        topHandler.RemoveLowestChild();
    }

    public void AddLowestChild(InputHandler child)
    {
        topHandler.AddLowestChild(child);
    }

    public void ReplaceLowestWithNew(InputHandler newHandler)
    {
        RemoveLowestChild();
        AddLowestChild(newHandler);
    }

    public void RemoveAllHandlers()
    {
        topHandler.RemoveChild();
    }

    public bool IsGUIInput()
    {
        return isGuiInput;
    }

    public void SetGUIInput()
    {
        isGuiInput = true;
    }

    public void SetNormalInput()
    {
        isGuiInput = false;
    }

    private void debug(string msg)
    {
        Debug.Log("[TouchDebug] " + msg);
    }
}
