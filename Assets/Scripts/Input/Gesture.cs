﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Gesture {

    private const float TAP_MAX_TIME = 0.15f;

    public List<TouchInfo> touches;
    
    public Gesture()
    {
        touches = new List<TouchInfo>();
    }

    public void Clear()
    {
        touches.Clear();
    }

    public void AddTouch(Vector2 pos, float time)
    {
        touches.Add(new TouchInfo(pos, time));
    }

    public float Magnitude
    {
        get
        {
            if (touches.Count == 0) 
                return 0;

            Vector2 min = touches[0].position;
            Vector2 max = touches[0].position;

            foreach (TouchInfo touch in touches)
            {
                if (touch.position.x < min.x)
                    min.x = touch.position.x;
                if (touch.position.x > max.x)
                    max.x = touch.position.x;

                if (touch.position.y < min.y)
                    min.y = touch.position.y;
                if (touch.position.y > max.y)
                    max.y = touch.position.y;
            }

            return (max - min).magnitude;
        }
    }

    public bool IsTapMap
    {
        get
        {
            if (Time > TAP_MAX_TIME)
                return false;

            Vector2i cell = new Vector2i(1488, 1488);
            foreach (TouchInfo touch in touches)
            {
                RaycastHit hit;

                if (!RaycastController.RaycastMap(RaycastController.CursorToRay(touch.position), out hit))
                    return false;

                Vector2i foundCell = GameController.Map.MapCell(GameController.Map.WorldToMap(hit.point));

                if (cell.x == 1488 && cell.y == 1488) // if not inited
                    cell = foundCell;
                else if (cell.x != foundCell.x || cell.y != foundCell.y) // if any position in gesture is out of first touched position
                    return false;
            }

            return true;
        }
    }

    public float Time
    {
        get
        {
            return touches.Count == 0 ? 0.0f : (touches[touches.Count - 1].time - touches[0].time);
        }
    }
	
    public class TouchInfo
    {
        public Vector2 position;
        public float time;

        public TouchInfo(Vector2 pos, float time)
        {
            this.position = pos;
            this.time = time;
        }
    }
}
