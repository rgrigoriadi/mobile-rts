﻿using UnityEngine;
using System.Collections;

public abstract class AResourceController : AmountLabel.IAmmountLabelable 
{

    public Player Player;

    public abstract void Add(float amount);
    public abstract void Substract(float amount);

    public abstract bool Has(float amount);
    public abstract void OnAmountUpdate();

    public abstract bool CanShowResources();
    public abstract float GetAmount();

    public abstract int GetValue();
}
