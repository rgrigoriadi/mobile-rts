﻿using UnityEngine;
using System.Collections;

public class LimitedResourceController : AResourceController {

    private float amount;
    
    public LimitedResourceController (float amount)
    {
        this.amount = amount;
    }

    public override void Add(float amount)
    {
        this.amount += amount;
        if (amount != 0.0f)
            OnAmountUpdate();
    }

    public override void Substract(float amount)
    {
        if (Has(amount))
        {
            this.amount -= amount;
            if (amount != 0.0f)
                OnAmountUpdate();
        }
        else
        {
            if (Player == GameController.Diplomacy.CurrentPlayer)
                GameController.GUI.OnCantSpendResourcesError();
            Debug.LogError(Player.Name + " can't spend " + amount + " not enough resources");
        }
    }

    public override bool Has(float amount)
    {
        return amount <= this.amount;
    }

    public override void OnAmountUpdate()
    {
        //if (Player == GameController.Diplomacy.CurrentPlayer)
        //{
        //    GameController.GUI.OnResourceChange(this.amount);
        //}
    }

    public override bool CanShowResources()
    {
        return true;
    }

    public override float GetAmount()
    {
        return amount;
    }

    public override int GetValue()
    {
        return (int)amount;
    }
}
