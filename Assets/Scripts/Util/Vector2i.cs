﻿using UnityEngine;
using System.Collections;

public class Vector2i {
	
	private Vector2 data;
	
	public int x 
	{
		get 
		{
			return Mathf.RoundToInt(data.x);
		}
		
		set
		{
			data.x = Mathf.RoundToInt(value);
		}
	}   

	public int y 
	{
		get 
		{
			return Mathf.RoundToInt(data.y);
		}
		
		set
		{
			data.y = Mathf.RoundToInt(value);
		}
	}

    public static Vector2i zero
    {
        get
        {
            return new Vector2i(0, 0);
        }
    }

	public Vector2i(int x = 0, int y = 0) 
	{
		this.x = x;
		this.y = y;
	}

    public Vector2i(Vector2 vector)
    {
        this.x = Mathf.RoundToInt(vector.x);
        this.y = Mathf.RoundToInt(vector.y);
    }
	
	public Vector2 ToVector2 
	{
		get 
		{
			return data;
		}
	}

    public static Vector2i operator +(Vector2i op1, Vector2i op2)
    {
        return new Vector2i(op1.x + op2.x, op1.y + op2.y);
    }

    public static Vector2i operator -(Vector2i op1, Vector2i op2)
    {
        return new Vector2i(op1.x - op2.x, op1.y - op2.y);
    }

    public static Vector2 operator *(Vector2i op1, float f)
    {
        return new Vector2(op1.x * f, op1.y * f);
    }

    public static Vector2 operator /(Vector2i op1, float f)
    {
        return new Vector2(op1.x / f, op1.y / f);
    }

    public bool Equals(Vector2i op)
    {
        return op == null ? false : op.x == x && op.y == y;
    }

    public bool IsZero()
    {
        return Equals(Vector2i.zero);
    }

    public override string ToString()
    {
        return "(" + x + ", " + y + ")";
    }
}
