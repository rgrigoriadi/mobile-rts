﻿using UnityEngine;
using System.Collections;

public abstract class AShootEffect : MonoBehaviour {

    public abstract void show (Vector3 start, Vector3 target);

}
