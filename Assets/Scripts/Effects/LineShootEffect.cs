﻿using UnityEngine;
using System.Collections;

public class LineShootEffect : AShootEffect {

    public float LineWidth;
    public float EffectTime;

    private float createTime = float.NaN;

	void Start () {
	
	}
	
	void Update () {
        if (!float.IsNaN(createTime))
        {
            if (Time.time > createTime + EffectTime)
            {
                GameObject.Destroy(gameObject);
            }
        }
	}

    public override void show(Vector3 start, Vector3 target)
    {
        createTime = Time.time;

        transform.position = (start + target) / 2.0f;

        Vector3 size = GetComponent<MeshRenderer>().bounds.size;

        Vector3 targetSize = new Vector3((target - start).magnitude, LineWidth, LineWidth);

        Vector3 scale = transform.localScale;
        scale.Scale(new Vector3(targetSize.x / size.x, targetSize.y / size.y, targetSize.z / size.z));

        transform.localScale = scale;

        Vector3 dpos = target - start;

        transform.rotation = Quaternion.Euler(0.0f, 0.0f, Mathf.Atan2(dpos.y, dpos.x) * Mathf.Rad2Deg);
            
            //Quaternion.FromToRotation(start, target);
    }

    private void resize()
    {
        
    }
}
