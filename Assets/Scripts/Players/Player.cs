﻿using UnityEngine;
using System.Collections.Generic;

public class Player {

    private int playerIdx;
    private Team comand;

    private int unitCount = 0;

    private bool isActive = true;
    public bool IsActive { get { return isActive; } }

    private string name;
    public string Name { get { return name; } }

    AResourceController resources;
    IAI ai;

    public Team Comand
    {
        get { return comand; }
        set { comand = value; }
    }

    public Player(int index, string name, AResourceController resourceController, IAI aiClass)
    {
        this.playerIdx = index;
        this.name = name;
        this.resources = resourceController;
        this.ai = aiClass;
        this.ai.SetPlayer(this);

        resourceController.Player = this;
    }

    public bool isAllyOf(Player player)
    {
        return comand.IsAllyOf(player.comand);
    }

    public bool isEnemyOf(Player player)
    {
        return comand.IsEnemyOf(player.comand);
    }

    public bool isNeutral()
    {
        return comand.IsNeutral;
    }

    public int Index
    {
        get { return playerIdx; }
    }

    public void Update()
    {
        ai.Update();
    }

    public void OnAddUnit()
    {
        unitCount++;
    }

    public void OnRemoveUnit()
    {
        unitCount--;
        if (unitCount == 0)
            OnAllUnitsDestroyed();
    }

    public virtual void OnAllUnitsDestroyed()
    {
        isActive = false;
        comand.OnPlayerDestroyed(this);
    }

    public float GetResources()
    {
        return resources.GetAmount();
    }

    public AResourceController GetResourceController()
    {
        return resources;
    }

    public List<Unit> GetAllUnits()
    {
        Unit[] allUnits = GameController.UnitController.Units;
        List<Unit> ret = new List<Unit>();

        for (int i = 0; i < allUnits.Length; i++)
            if (allUnits[i].Player == this)
                ret.Add(allUnits[i]);
        return ret;
    }

    public bool HasUnit(UnitFactory.UnitID id)
    {
        List<Unit> myUnits = GetAllUnits();

        foreach (Unit unit in myUnits)
            if (unit.UID == id)
                return true;
        return false;
    }

}