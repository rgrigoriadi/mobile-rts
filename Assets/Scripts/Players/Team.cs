﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Team {

    private List<Player> players;

    private bool isActive = true;
    public bool IsActive { get { return isActive; } }

    private bool isNeutral;
    public bool IsNeutral { get { return isNeutral; } }

    public Team(bool isNeutral) 
    {
        players = new List<Player>();
        this.isNeutral = isNeutral;
    }

    public Team AddPlayer(Player player)
    {
        players.Add(player);
        player.Comand = this;
        return this;
    }

    public bool IsAllyOf(Team comand) 
    {
        return comand == this;
    }

    public bool IsEnemyOf(Team comand)
    {
        return !IsAllyOf(comand) && !comand.IsNeutral && !isNeutral;
    }

    public List<Player> GetPlayers()
    {
        return players;
    }

    public void OnPlayerDestroyed(Player player)
    {
        if (!HasActivePlayers)
            OnComandDestroyed();
    }

    private void OnComandDestroyed()
    {
        isActive = false;
        GameController.Diplomacy.OnComandDestroyed(this);
    }

    public bool HasActivePlayers
    {
        get
        {
            foreach (Player player in players)
                if (player.IsActive)
                    return true;
            return false;
        }
    }

}
