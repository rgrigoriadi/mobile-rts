﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController 
{
    public const int INITIAL_RESOURCES = 200;

    public static Player DEFAULT_YOU = new Player(0, "You", new LimitedResourceController(INITIAL_RESOURCES), new AIDoNothing());
    public static Player DEFAULT_ENEMY = new Player(1, "Enemy", new LimitedResourceController(INITIAL_RESOURCES), new AIBasicDestroyNearest());//new AIDoNothing());
    public static Player DEFAULT_NEUTRAL = new Player(2, "Neutral", new LimitedResourceController(INITIAL_RESOURCES), new AIDoNothing());

    private Team[] comands;

    private Player currentPlayer;
    public Player CurrentPlayer {
        get { return currentPlayer; }
        set { 
            currentPlayer = value;
            GameController.GUI.OnPlayerChange(value);
        }
    }

    public PlayerController()
    {

    }

    public void InitDefault()
    {
        Init(
            new Team[3]
            {
                new Team(false).AddPlayer(DEFAULT_YOU), 
                new Team(false).AddPlayer(DEFAULT_ENEMY),
                new Team(true).AddPlayer(DEFAULT_NEUTRAL)
            }, 
            DEFAULT_YOU);
    }

    public void Init(Team[] comands, Player currentPlayer)
    {
        this.comands = comands;
        CurrentPlayer = currentPlayer;
    }

    public void Update() {
        if (comands != null)
            foreach (Team com in comands)
                foreach(Player player in com.GetPlayers())
                    player.Update();
    }

    public Player GetPlayer(int index)
    {
        foreach (Team comand in comands)
            foreach (Player player in comand.GetPlayers())
                if (player.Index == index)
                    return player;
        return null;
    }

    public void OnComandDestroyed(Team team)
    {
        Team winner = GetWinner;
        if (winner != null)
            GameController.Level.OnLevelEnd(winner);
    }

    public Team GetWinner
    {
        get
        {
            int numberOfActiveCommands = 0;
            Team winner = null;
            foreach (Team comand in comands)
                if (comand.IsActive)
                {
                    numberOfActiveCommands++;
                    winner = comand;
                }
            if (numberOfActiveCommands == 1)
                return winner;
            else
                return null;
        }
    }
}
