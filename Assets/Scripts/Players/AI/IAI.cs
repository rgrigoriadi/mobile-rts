﻿using UnityEngine;
using System.Collections;

public interface IAI {
    void Update();
    void SetPlayer(Player p);

    // EVENT CATCHERS SHOULD BE DOWN THERE

    // START GAME
    // ENEMY UNIT IN RANGE
    // ENEMY UNIT ATTACKED US
    // WE ATTACKED ENEMY UNIT

    // OUR UNIT DESTROYED
    // OUR BUILDING DESTROYED
}
