﻿using UnityEngine;
using System.Collections.Generic;

public class AIBasicDestroyNearest : IAI {

    Dictionary<UnitFactory.UnitID, float> UIDCost = new Dictionary<UnitFactory.UnitID, float>()
    {
        {UnitFactory.UnitID.MARINE,             1f},
        {UnitFactory.UnitID.MERCENARY,          1.25f},
        {UnitFactory.UnitID.APC,                1.75f},
        {UnitFactory.UnitID.LIGHT_ASSAULT_BOT,  1.75f},
        {UnitFactory.UnitID.WARDOG,             2.0f},
        {UnitFactory.UnitID.TANK,               2.75f},
        {UnitFactory.UnitID.PROJECT_GRAVITY,    3.0f},

        {UnitFactory.UnitID.HELI,               1.5f},
        {UnitFactory.UnitID.QUADROCOPTER,       2.5f},
    };

    Dictionary<UnitFactory.UnitID, float> UIDProb = new Dictionary<UnitFactory.UnitID, float>()
    {
        {UnitFactory.UnitID.MARINE,             0.555f},    // 0.555
        {UnitFactory.UnitID.MERCENARY,          0.125f},    // 0.680
        {UnitFactory.UnitID.APC,                0.075f},
        {UnitFactory.UnitID.LIGHT_ASSAULT_BOT,  0.075f},    // 0,830
        {UnitFactory.UnitID.WARDOG,             0.025f},    // 0.855
        {UnitFactory.UnitID.TANK,               0.015f},    // 0.87 
        {UnitFactory.UnitID.PROJECT_GRAVITY,    0.010f},    // 0.88

        {UnitFactory.UnitID.HELI,               0.1f},      // 0.1
        {UnitFactory.UnitID.QUADROCOPTER,       0.02f},     // 0.12
    };

    Player player;

    public Vector2i pos
    {
        get
        {
            return new Vector2i(GameController.Map.MapSize.x - 6, GameController.Map.MapSize.y / 2);
        }
    }

    public const float SPAWN_INTERVAL = 10f;//45f;
    public float lastSpawnTime = 0f;

    public float prevPrevLastSpawnCost = 0f;
    public float prevLastSpawnCost = 1f;

    public void Update()
    {
        TryToSpawn();

        CommandToAttack();
    }

    private void TryToSpawn()
    {
        if (Time.time - lastSpawnTime > SPAWN_INTERVAL)
        {
            lastSpawnTime += SPAWN_INTERVAL;
            float spawnCost = prevLastSpawnCost + 1;//prevLastSpawnCost + prevPrevLastSpawnCost;

            prevPrevLastSpawnCost = prevLastSpawnCost;
            prevLastSpawnCost = spawnCost;

            UnitFactory.UnitID uid = UnitFactory.UnitID.MARINE;

            Vector2i position = GameController.Map.GetFreePositionCenterCloseTo(pos.ToVector2, GameController.UnitController.SIZE_BY_UID()[uid]);

            for (int i = 0; i < spawnCost; i++)
                AIUtils.SpawnIt(uid, position, PlayerController.DEFAULT_ENEMY, null);
        }
    }

    private void CommandToAttack()
    {
        List<Unit> units = player.GetAllUnits();

        foreach (Unit unit in units)
        {
            if (unit.ActiveObject != null && !unit.ActiveObject.IsBusy() && unit.AbleToAtack != null)
            {
                Unit nearestEnemy = GameController.DistanceMap.GetNearestEnemyUnitInRangeOf(unit, float.PositiveInfinity);
                if (nearestEnemy != null)
                    if (nearestEnemy.HPController != null)
                    {
                        unit.AbleToAtack.Atack(nearestEnemy.HPController, false);
                    }
            }
        }
    }

    public void SetPlayer(Player p)
    {
        this.player = p;
    }
}