﻿using UnityEngine;
using System.Collections.Generic;

public class AIUtils {

    public static void SpawnEmAll(List<UnitFactory.UnitID> uids, Vector2i position, Player p, ObjectToBuild obj)
    {
        foreach (UnitFactory.UnitID uid in uids)
            SpawnIt(uid, position, p, obj);
    }

    public static void SpawnIt(UnitFactory.UnitID uid, Vector2i position, Player p, ObjectToBuild obj)
    {
        GameController.UnitController.CreateObject(uid, position, p, obj);
    }
}
