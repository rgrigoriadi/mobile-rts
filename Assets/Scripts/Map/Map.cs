using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Map : MonoBehaviour
{

    private Vector2i mapSize;

    private Unit[,] unitAnchors;

    public MapGridController GridController
    { get { return (MapGridController)GameObject.FindObjectOfType(typeof(MapGridController)); } }

    public AMapLoader Loader;// = new AMapLoader();

    public Vector2i MapSize
    {
        get { return mapSize; }

        set
        {
            mapSize = value;
            unitAnchors = new Unit[mapSize.x, mapSize.y];
        }
    }

    public void LoadLevel(AMapLevel level)
    {
        LoadEmptyLevel(level.GetSizeInCells().x, level.GetSizeInCells().y);

        TestMapLoader.fillTestMap(this);
    }

    public void LoadTestLevel()
    {
        LoadEmptyLevel(96, 12);
        TestMapLoader.fillTestMap(this);
    }

    private void LoadEmptyLevel(int width, int height)
    {
        cleanMap();

        MapSize = new Vector2i(width, height);
        ((MapBack)GameObject.FindObjectOfType(typeof(MapBack))).SetSize(mapSize);

        GridController.ShowGrid(this);

        GameController.Camera.InitCam(MapSize);
    }

    private void cleanMap()
    {
        Unit[] units = GameController.UnitController.Units;
        foreach (Unit unit in units)
            GameObject.Destroy(unit.gameObject);
    }

    public float HeightAt(Vector2 posititon)
    {
        RaycastHit hit = new RaycastHit();

        if (RaycastController.RaycastMap(new Ray(new Vector3(posititon.x, posititon.y, -1000), new Vector3(0.0f, 0.0f, 1.0f)), out hit))
            return hit.point.z;
        else
            return 0.0f;
    }

    public Vector2 WorldToMap(Vector2 worldCoord)
    {
        return worldCoord;
    }

    public Vector2i MapCell(Vector2 mapCoord)
    {
        return new Vector2i((int)mapCoord.x, (int)mapCoord.y);
    }

    public Vector2 MapToWorld(Vector2 mapCoord)
    {
        return mapCoord;
    }

    public Unit GetUnitAt(Vector2i mapPos)
    {
        return unitAnchors[mapPos.x, mapPos.y];
    }

    public Unit GetAnyUnitAt(Vector2i leftBottomPos, Vector2i size)
    {
        for (int x = 0; x < size.x; x++)
            for (int y = 0; y < size.y; y++)
            {
                Unit unit = GetUnitAt(new Vector2i((int)(leftBottomPos.x + x), (int)(leftBottomPos.y + y)));
                if (unit != null)
                    return unit;
            }
        return null;
    }

    public Unit GetAnyUnitAt(Vector2i leftBottomPos, Vector2i size, Unit unitToExcept)
    {
        for (int x = 0; x < size.x; x++)
            for (int y = 0; y < size.y; y++)
            {
                Unit unit = GetUnitAt(new Vector2i((int)(leftBottomPos.x + x), (int)(leftBottomPos.y + y)));
                if (unit != null && unit != unitToExcept)
                    return unit;
            }
        return null;
    }

    public bool IsBusy(Vector2i cell)
    {
        return GetUnitAt(cell) != null;
    }

    public bool IsBusy(Vector2i leftBottomPos, Vector2i size)
    {
        return GetAnyUnitAt(leftBottomPos, size) != null;
    }

    public bool IsBusy(Vector2i leftBottomPos, Vector2i size, Unit unitToExcept)
    {
        return GetAnyUnitAt(leftBottomPos, size, unitToExcept) != null;
    }

    //public bool IsAvailableForUnits(Vector2i cell)
    //{
    //    if (cell.x < 0 || cell.x >= MapSize.x || cell.y < 0 || cell.y >= MapSize.y)
    //        return false;
    //    return !IsBusy(cell);
    //}

    public void RemoveUnitFromCell(Vector2i cell)
    {
        if (IsBusy(cell))
            unitAnchors[cell.x, cell.y] = null;
    }

    private void PutUnitToCell(Vector2i cell, Unit unit)
    {
        if (!IsBusy(cell))
            unitAnchors[cell.x, cell.y] = unit;
        else
            if (GetUnitAt(cell) != unit)
                Debug.LogError("Cannot put " + unit.Name + " to " + cell.ToString() + ", " + unitAnchors[cell.x, cell.y].Name + " is there");
    }

    public void RemoveUnit(Unit unit)
    {
        List<Vector2i> occupiedPositions = unit.Position.GetOccupiedCellsList();
        foreach (Vector2i cell in occupiedPositions)
            RemoveUnitFromCell(cell);
    }

    public void PutUnit(Unit unit)
    {
        PutUnitShifted(Vector2i.zero, unit);
    }

    public void PutUnitShifted(Vector2i positionShift, Unit unit)
    {
        List<Vector2i> occupiedPositions = unit.Position.GetOccupiedCellsList();

        foreach (Vector2i cell in occupiedPositions)
            RemoveUnitFromCell(cell);

        foreach (Vector2i cell in occupiedPositions)
            PutUnitToCell(cell + positionShift, unit);
    }

    public Vector2i GetFreePositionCenterCloseTo(Vector2 posForCenter, Vector2i resultSize)
    {
        Vector2 bottomLeft = posForCenter - resultSize.ToVector2 / 2.0f;

        Vector2i roundedBottomLeft = new Vector2i(Mathf.RoundToInt(bottomLeft.x), Mathf.RoundToInt(bottomLeft.y));

        if (IsInBounds(roundedBottomLeft, resultSize) && !GameController.Map.IsBusy(roundedBottomLeft, resultSize))
            return roundedBottomLeft;
        else
            return GetFreePositionAround(roundedBottomLeft, new Vector2i(0, 0), resultSize, true);
    }

    public Vector2i GetFreePositionAround(Vector2i bottomLeft, Vector2i blockedSize, Vector2i resultSize, bool noBldgMode) 
        // last arg just to find closest leftBottom point mode
    {
        // For left bottom
        bool isDebug = false;

        Vector2i bldgPos = bottomLeft;
        Vector2i bldgSize = blockedSize;
        Vector2i size = resultSize;

        Vector2 center = bottomLeft.ToVector2 + blockedSize.ToVector2 / 2.0f;

        int maxDist = 8;    // TODO if there is no place on map

        int left, right, left1, right1, bottom, top; // l1 r1 for limitimg bottom and top lines of search
            // |-----|
            // ||---||
            // ||xxx||
            // ||xxx||
            // ||---||
            // |-----|

        if (noBldgMode)
        {
            left = (int)bldgPos.x;
            left1 = (int)bldgPos.x;

            right = (int)bldgPos.x;
            right1 = (int)bldgPos.x;

            bottom = (int)bldgPos.y;
            top = (int)bldgPos.y;
        }
        else
        {
            left = (int)(bldgPos.x - size.x);
            right = (int)(bldgPos.x + bldgSize.x);

            left1 = (int)(bldgPos.x);
            right1 = (int)(bldgPos.x + bldgSize.x - size.x); if (right1 < left1) right1 = left1;

            bottom = (int)(bldgPos.y - size.y);
            top = (int)(bldgPos.y + bldgSize.y);
        }

        for (int row = 1; row < maxDist; row++)
        {
            if (left   <  0)              left   = 0;
            if (left1  <  0)              left1  = 0;
            if (right < 0) right = 0;
            if (right1 < 0) right = 0;
            if (left >  this.MapSize.x - size.x) left = (int)this.MapSize.x - size.x;
            if (left1 > this.MapSize.x - size.x) left1 = (int)this.MapSize.x - size.x;
            if (right  > this.MapSize.x - size.x) right  = (int)this.MapSize.x - size.x;
            if (right1 > this.MapSize.x - size.x) right1 = (int)this.MapSize.x - size.x;
            if (bottom < 0) bottom = 0;
            if (top    < 0) top = 0;
            if (bottom > this.MapSize.y - size.y) bottom = (int)this.MapSize.y - size.y;
            if (top > this.MapSize.y - size.y) top = (int)this.MapSize.y - size.y;

            Vector2i closestPoint = null;
            float closestDist = float.PositiveInfinity;

            if (isDebug) Debug.Log("Round " + row + " " + left + " " + left1 + " " + right + " " + right1 + " | " + bottom + " " + top);

            if (isDebug) Debug.Log("Bottom go");
            for (int i = left1, j = bottom; i <= right1; i++)
            {
                Vector2i pos = new Vector2i(i, j);
                if (!IsBusy(pos, size))
                {
                    float dist = Vector2.Distance(center, pos.ToVector2);
                    if (dist < closestDist)
                    {
                        closestPoint = pos;
                        closestDist = dist;
                    }
                }
            }

            if (isDebug) Debug.Log("Right go");
            for (int i = right, j = bottom; j <= top; j++)
            {
                Vector2i pos = new Vector2i(i, j);
                if (!IsBusy(pos, size))
                {
                    float dist = Vector2.Distance(center, pos.ToVector2);
                    if (dist < closestDist)
                    {
                        closestPoint = pos;
                        closestDist = dist;
                    }
                }
            }

            if (isDebug) Debug.Log("Top go");
            for (int i = left1, j = top; i <= right1; i++)
            {
                Vector2i pos = new Vector2i(i, j);
                if (!IsBusy(pos, size))
                {
                    float dist = Vector2.Distance(center, pos.ToVector2);
                    if (dist < closestDist)
                    {
                        closestPoint = pos;
                        closestDist = dist;
                    }
                }
            }

            if (isDebug) Debug.Log("Left go");
            for (int i = left, j = bottom; j <= top; j++)
            {
                Vector2i pos = new Vector2i(i, j);
                if (!IsBusy(pos, size))
                {
                    float dist = Vector2.Distance(center, pos.ToVector2);
                    if (dist < closestDist)
                    {
                        closestPoint = pos;
                        closestDist = dist;
                    }
                }
            }

            if (closestPoint != null)
                return closestPoint;

            left--;
            right++;
            left1--;
            right1++;
            top++;
            bottom--;
        }

        return null;
    }

    public bool IsInBounds(Vector2i pos)
    {
        return pos.x >= 0 && pos.x < MapSize.x && pos.y >= 0 && pos.y < MapSize.y;
    }

    public bool IsInBounds(Vector2i bottomLeft, Vector2i size)
    {
        Vector2i pos = bottomLeft;
        return pos.x >= 0 && pos.x + size.x - 1 < MapSize.x && pos.y >= 0 && pos.y + size.y - 1 < MapSize.y;
    }

}
