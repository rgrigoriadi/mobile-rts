﻿using UnityEngine;
using System.Collections;

public class MapGridController : MonoBehaviour {

	public Transform MapGridLine;
	
	void Start () {
	
	}
	
	void Update () {
	
	}
	
	public void ShowGrid(Map map) 
	{
		DestroyGrid();
		buildGrid(map);
	}
	
	private void buildGrid(Map map) 
	{
		for (int i = 1; i < map.MapSize.x; i++)
			createLine().SetVertical(i, map.MapSize.y);
				
		for (int j = 1; j < map.MapSize.y; j++) 
			createLine().SetHorizontal(j, map.MapSize.x);
	}
	
	private MapGridLine createLine() 
	{
		MapGridLine ret = ((Transform) GameObject.Instantiate(MapGridLine)).GetComponent<MapGridLine>();
		ret.transform.parent = transform;
        ret.transform.localPosition = Vector3.zero;
		return ret;
	}
	
	public void DestroyGrid() 
	{
		GameObject[] gridLines = GameObject.FindGameObjectsWithTag("MapGridElement");
		
		foreach (GameObject line in gridLines) 
		{
			GameObject.Destroy(line);
		}
	}
}
