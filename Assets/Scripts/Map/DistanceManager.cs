﻿using UnityEngine;
using System.Collections.Generic;

public interface IDistanceManager
{
    void OnAdd(Unit unit);
    void UpdateFor(Unit unit);
    float GetRangeBetween(Unit unit, Unit target);
    float GetRangeBetween(Vector2i unitPos, Vector2i unitSize, Vector2i targetPos, Vector2i targetSize);
    Unit GetNearestEnemyUnitInRangeOf(Unit unit, float range);
    Unit[] GetEnemyUnitsInRangeOf(Unit unit, float range);
    Unit[] GetAnyUnitsNonNeutralInRangeOf(Unit unit, float range);
    void OnRemove(Unit unit);
}


public class SimpleDistanceManager : IDistanceManager
{

    public void OnAdd(Unit unit)
    {
        // DONOTHING
    }

    public void UpdateFor(Unit unit)
    {
        // DONOTHING
    }

    public float GetRangeBetween(Unit unit, Unit target)
    {
        return GetRangeBetween(unit.Position.BottomLeftOnMapMap, unit.Size, target.Position.BottomLeftOnMapMap, target.Size);
    }

    public float GetRangeBetween(Vector2i unitPos, Vector2i unitSize, Vector2i targetPos, Vector2i targetSize)
    {
        float xDist = 0;
        float yDist = 0;

        if (unitPos.x > targetPos.x + targetSize.x)
            xDist = unitPos.x - (targetPos.x + targetSize.x);
        else if (unitPos.x + unitSize.x < targetPos.x)
            xDist = targetPos.x - (unitPos.x + unitSize.x);
        else
            xDist = 0;

        if (unitPos.y > targetPos.y + targetSize.x)
            yDist = unitPos.y - (targetPos.y + targetSize.y);
        else if (unitPos.y + unitSize.y < targetPos.y)
            yDist = targetPos.y - (unitPos.y + unitSize.y);
        else
            yDist = 0;

        return new Vector2(xDist, yDist).magnitude;
    }

    public Unit GetNearestEnemyUnitInRangeOf(Unit unit, float range)
    {
        Unit[] units = GetEnemyUnitsInRangeOf(unit, range);

        if (units != null)
        {
            float minRange = float.PositiveInfinity;
            Unit nearest = null;
            for (int i = 0; i < units.Length; i++)
            {
                float dist = GetRangeBetween(unit, units[i]);
                if (dist < minRange || nearest == null)
                {
                    minRange = dist;
                    nearest = units[i];
                }
            }
            return nearest;
        }
        else
            return null;

    }

    public Unit[] GetEnemyUnitsInRangeOf(Unit unit, float range)
    {
        Unit[] units = GameController.UnitController.Units;

        List<Unit> ret = new List<Unit>();
        foreach (Unit otherUnit in units)
            if (unit.Player.isEnemyOf(otherUnit.Player))
                if (GetRangeBetween(unit, otherUnit) < range)
                    if (unit != otherUnit)
                        ret.Add(otherUnit);

        return ret.ToArray();
    }

    public Unit[] GetAnyUnitsNonNeutralInRangeOf(Unit unit, float range)
    {
        Unit[] units = GameController.UnitController.Units;

        List<Unit> ret = new List<Unit>();
        foreach (Unit otherUnit in units)
            if (GetRangeBetween(unit, otherUnit) <= range)
                if (unit != otherUnit)
                    ret.Add(otherUnit);

        return ret.ToArray();
    }

    public void OnRemove(Unit unit)
    {
        // DONOTHING
    }
}
    
    //public class MatrixDistanceManager : IDistanceManager
    //{
    //    // ЭН КВАДРАТ памяти
    //    // при добавлении / удалении объекта - 2 ЭН операций со списками
    //    // при чтении ЭН операций просмотра
    //    // при чтении НОЛЬ операций пересчета расстояний

    //    // собрать динамическую матрицу, как двумерный лист (с ссылкой на след снизу и справа - быстрее будет обновлять

    //    // Либо апдейтить всю строку и столбец либо / Если нижняя оценка расстояния больше дистанции боя - не считать
    //    public void OnAdd(Unit unit)
    //    {

    //    }

    //    public void UpdateFor(Unit unit)
    //    {

    //    }

    //    public Unit[] GetEnemyUnitsInRangeOf(Unit unit, float range)
    //    {

    //        return null;
    //    }

    //    public void OnRemove(Unit unit)
    //    {

    //    }
    //}

    //public class ListDistanceManager : IDistanceManager
    //{
    //    // ЭН памяти
    //    // Добавлять и удалять объект - ОДНА операция со списком
    //    // при чтении 1/5 ЭН операций просмотра
    //    // при чтении 1/5 ЭН операций пересчета расстояний

    //    // список списков - верхний - ряды, в которых есть юниты, внутренние - столбцы


    //    public void OnAdd(Unit unit)
    //    {

    //    }

    //    public void UpdateFor(Unit unit)
    //    {

    //    }

    //    public Unit[] GetEnemyUnitsInRangeOf(Unit unit, float range)
    //    {

    //        return null;
    //    }

    //    public void OnRemove(Unit unit)
    //    {

    //    }
    //}

    //// Если юниты добавляются и удаляются нечасто - выгоднее матрица
    //// Кешировать лист?


