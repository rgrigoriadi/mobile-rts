﻿using UnityEngine;
using System.Collections;

public class TestMapLoader : MonoBehaviour {

	void Start () {
	
	}
	
	void Update () {
	
	}

    public static void fillTestMap(Map map)
    {
        GameController.UnitController.CreateObject(UnitFactory.UnitID.ENERGY_POINT, new Vector2i(12, 2), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.ENERGY_POINT, new Vector2i(12, 9), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.ENERGY_POINT, new Vector2i(31, 6), PlayerController.DEFAULT_NEUTRAL, null);

        GameController.UnitController.CreateObject(UnitFactory.UnitID.ENERGY_POINT, new Vector2i(46, 2), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.ENERGY_POINT, new Vector2i(46, 9), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.ENERGY_POINT, new Vector2i(49, 2), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.ENERGY_POINT, new Vector2i(49, 9), PlayerController.DEFAULT_NEUTRAL, null);

        GameController.UnitController.CreateObject(UnitFactory.UnitID.ENERGY_POINT, new Vector2i(64, 5), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.ENERGY_POINT, new Vector2i(83, 2), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.ENERGY_POINT, new Vector2i(83, 9), PlayerController.DEFAULT_NEUTRAL, null);

        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(19, 2), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(19, 3), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(19, 8), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(19, 9), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(22, 5), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(22, 6), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(38, 4), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(38, 5), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(38, 6), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(38, 7), PlayerController.DEFAULT_NEUTRAL, null);

        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(42, 1), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(43, 1), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(44, 1), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(42, 2), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(42, 3), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(42, 8), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(42, 9), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(42, 10), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(43, 10), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(44, 10), PlayerController.DEFAULT_NEUTRAL, null);

        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(76, 2), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(76, 3), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(76, 8), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(76, 9), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(73, 5), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(73, 6), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(57, 4), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(57, 5), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(57, 6), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(57, 7), PlayerController.DEFAULT_NEUTRAL, null);

        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(53, 1), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(52, 1), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(51, 1), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(53, 2), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(53, 3), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(53, 8), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(53, 9), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(53, 10), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(52, 10), PlayerController.DEFAULT_NEUTRAL, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.STONE, new Vector2i(51, 10), PlayerController.DEFAULT_NEUTRAL, null);


        GameController.UnitController.CreateObject(UnitFactory.UnitID.TANK, new Vector2i(4, 5), GameController.Diplomacy.CurrentPlayer, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.LIGHT_ASSAULT_BOT, new Vector2i(4, 3), GameController.Diplomacy.CurrentPlayer, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.LIGHT_ASSAULT_BOT, new Vector2i(4, 8), GameController.Diplomacy.CurrentPlayer, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.MARINE, new Vector2i(7, 4), GameController.Diplomacy.CurrentPlayer, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.MERCENARY, new Vector2i(7, 5), GameController.Diplomacy.CurrentPlayer, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.MARINE, new Vector2i(7, 7), GameController.Diplomacy.CurrentPlayer, null);


        GameController.UnitController.CreateObject(UnitFactory.UnitID.LIGHT_ASSAULT_BOT, new Vector2i(90, 2), PlayerController.DEFAULT_ENEMY, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.LIGHT_ASSAULT_BOT, new Vector2i(90, 4), PlayerController.DEFAULT_ENEMY, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.LIGHT_ASSAULT_BOT, new Vector2i(90, 3), PlayerController.DEFAULT_ENEMY, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.LIGHT_ASSAULT_BOT, new Vector2i(90, 5), PlayerController.DEFAULT_ENEMY, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.LIGHT_ASSAULT_BOT, new Vector2i(92, 2), PlayerController.DEFAULT_ENEMY, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.LIGHT_ASSAULT_BOT, new Vector2i(92, 4), PlayerController.DEFAULT_ENEMY, null);
        GameController.UnitController.CreateObject(UnitFactory.UnitID.LIGHT_ASSAULT_BOT, new Vector2i(92, 6), PlayerController.DEFAULT_ENEMY, null);


    }
}
