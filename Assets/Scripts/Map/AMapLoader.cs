﻿using UnityEngine;
using System.Collections;

public abstract class AMapLoader 
{
    public AMapMesh MapMesh
    { get { return (AMapMesh) GameObject.FindObjectOfType(typeof(AMapMesh)); } }

    public void LoadLevel(int levelId = AMapLevel.DEFAULT_LEVEL) {
        AMapLevel level = GetLevel(levelId);
        initMapMesh(level);
        initMap(level);
    }

    protected abstract AMapLevel GetLevel(int levelId = AMapLevel.DEFAULT_LEVEL);

    protected void initMapMesh(AMapLevel level)
    {
        MapMesh.LoadMap(level);
    }

    protected void initMap(AMapLevel level)
    {
        GameController.Map.LoadLevel(level);
    }
}
