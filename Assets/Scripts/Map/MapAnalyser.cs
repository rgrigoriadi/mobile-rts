﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapAnalyser
{
    private const bool isDebug = false;

    private class PathPart
    {
        public Vector2i PreviousPoint;
        public float Length;

        public PathPart()
        {
            PreviousPoint = null;
            Length = float.MaxValue;
        }

        public PathPart(Vector2i previousPoint, float length)
        {
            PreviousPoint = previousPoint;
            Length = length;
        }

        public bool HasVisited
        {
            get
            {
                return Length != float.MaxValue;
            }
        }
    }

    private static List<Vector2i> FindBadPathFast(Map map, Vector2i from, Vector2i to)
    {
        List<Vector2i> path = new List<Vector2i>();

        while (from.ToVector2 != to.ToVector2)
        {
            int dx = to.x - from.x;
            int dy = to.y - from.y;

            int mdx = dx < 0 ? -1 : dx == 0 ? 0 : 1;
            int mdy = dy < 0 ? -1 : dy == 0 ? 0 : 1;

            Vector2i direction = new Vector2i(mdx, mdy);

            path.Add(direction);

            from = new Vector2i(from.x + mdx, from.y + mdy);
        }

        return path;
    }

    public static List<Vector2i> FindPath(Map map, Vector2i from, Vector2i to, Unit unit)
    {
        //List<string> l = new List<string>();
        //l.Add("1");
        //l.Add("2");
        //l.Insert(2, "3");
        //Debug.Log(l[l.Count - 1]);

        //return FindBadPathFast(map, from, to);
        return AStar.AStarRun(map, from, to, unit);
    }

    public static List<Vector2i> FindPath(Map map, Unit from, Unit to, float radius, Unit unit)
    {
        return AStar.AStarRun(map, from, to, radius, unit);
    }

    public static List<Vector2i> FindPathBadAndSlow(Map map, Vector2i from, Vector2i to)
    {
        PathPart[,] pathMap = preparePathMap(map, from);

        pathMap = fillPathMap(pathMap, from);

        return buildPath(pathMap, from, to);
    }

    private static PathPart[,] fillPathMap(PathPart[,] pathMap, Vector2i position)
    {
        for (int i = -1; i < 2; i++)
            for (int j = -1; j < 2; j++)
                if (!(i == 0 && j == 0))
                {
                    Vector2i newPos = new Vector2i(position.x + i, position.y + j);

                    float dLen = new MoveDirection(new Vector2i(i, j)).Magnitude;

                    if (newPos.x < 0 || newPos.x >= pathMap.GetLength(0) || newPos.y < 0 || newPos.y >= pathMap.GetLength(1))
                    {
                        //DONOTHING
                    }
                    else if (pathMap[newPos.x, newPos.y].Length > pathMap[position.x, position.y].Length + dLen)
                    {
                        pathMap[newPos.x, newPos.y] = new PathPart(position, pathMap[position.x, position.y].Length + dLen);
                        fillPathMap(pathMap, newPos);
                    }
                    else
                    {
                        //DONOTHING
                    }
                }

        return pathMap;
    }

    private static PathPart[,] preparePathMap(Map map, Vector2i from)
    {
        PathPart[,] pathMap = new PathPart[map.MapSize.x, map.MapSize.y];

        for (int i = 0; i < pathMap.GetLength(0); i++)
            for (int j = 0; j < pathMap.GetLength(1); j++)
                pathMap[i, j] = new PathPart();

        pathMap[from.x, from.y] = new PathPart(Vector2i.zero, 0);

        return pathMap;
    }

    private static List<Vector2i> buildPath(PathPart[,] pathMap, Vector2i from, Vector2i to)
    {
        if (!pathMap[to.x, to.y].HasVisited)
            return null;

        List<Vector2i> path = new List<Vector2i>();
        Vector2i pos = to;

        while (pos.ToVector2 != from.ToVector2)
        {
            PathPart step = pathMap[pos.x, pos.y];

            path.Add(new Vector2i(pos.x - step.PreviousPoint.x, pos.y - step.PreviousPoint.y));

            pos = step.PreviousPoint;
        }

        path.Reverse();

        return path;
    }

    private class AStar
    {
        private class AStarNode
        {
            public Vector2i Pos;
            public float DistanceFromStart;
            public float DistanceToFinish;
            public AStarNode ParentNode;

            public AStarNode(Vector2i pos, float distFromStart, float dist2fin, AStarNode parent)
            {
                this.Pos = pos;
                this.DistanceFromStart = distFromStart;
                this.DistanceToFinish = dist2fin;
                this.ParentNode = parent;
            }

            public override string ToString()
            {
                return
                    Pos.ToString()
                    + " " + DistanceFromStart
                    + " -> " + DistanceToFinish
                    //+ " parent " + ParentNode != null && ParentNode.Pos != null ? ParentNode.Pos.ToString() : "null"
                    ;
            }
        }

        private static void insertSorted(List<AStarNode> nodes, AStarNode node)
        {
            int i = 0;
            for (i = 0; i < nodes.Count; i++)
            {
                if (node.DistanceToFinish < nodes[i].DistanceToFinish)
                {
                    nodes.Insert(i, node);
                    return;
                }
            }

            nodes.Insert(nodes.Count, node);
        }

        private static AStarNode Find(Vector2i point, List<AStarNode> list)
        {
            foreach (AStarNode node in list)
                if (point.Equals(node.Pos))
                    return node;
            return null;
        }

        private static List<Vector2i> Neighbours(Vector2i point, Vector2i size, Map map, Unit unit)
        {
            List<Vector2i> ret = new List<Vector2i>();
            for (int i = -1; i <= 1; i++)
                for (int j = -1; j <= 1; j++)
                {
                    Vector2i newPoint = new Vector2i(point.x + i, point.y + j);
                    if (!(i == 0 && j == 0) && GameController.Map.IsInBounds(newPoint, unit.Size))
                        if (!GameController.Map.IsBusy(newPoint, unit.Size, unit)) // TODO must depend in size
                            ret.Add(newPoint);
                }
            return ret;
        }

        public static List<Vector2i> AStarRun(Map map, Vector2i from, Vector2i to, Unit unit) // for left bottom corner
        {
            

            List<AStarNode> closed = new List<AStarNode>();
            List<AStarNode> opened = new List<AStarNode>();
            opened.Add(new AStarNode(from, 0.0f, shortestEstimate(from, to), null));
            //    closedset := the empty set    // The set of nodes already evaluated.
            //    openset := {start}    // The set of tentative nodes to be evaluated, initially containing the start node
            //    came_from := the empty map    // The map of navigated nodes.
            //    g_score[start] := 0    // Cost from start along best known path.
            //    // Estimated total cost from start to goal through y.
            //    f_score[start] := g_score[start] + heuristic_cost_estimate(start, goal)

            int maxIter = 20000;//int.MaxValue;

            bool okFinish = false;
            AStarNode finalNode = null;

            int i;
            for (i = 0; i < maxIter && opened.Count > 0; i++)
            {
                
                if (isDebug)
                {
                    string str = "List (" + opened.Count + ") = ";
                    for (int j = 0; j < opened.Count; j++)
                        str += "\n\t" + opened[j];
                    Debug.Log(str);
                }

                AStarNode current = opened[0];
                if (current.Pos.Equals(to))
                {
                    okFinish = true;
                    finalNode = current;
                    break;

                }

                //if (Vector2.Distance(current.Pos.ToVector2, to.ToVector2) < 2)
                if (isDebug) Debug.Log("Step " + i + " point main " + current + " target = " + to);


                opened.Remove(current);
                closed.Add(current);

                //    while openset is not empty
                //        current := the node in openset having the lowest f_score[] value
                //        if current = goal
                //            return reconstruct_path(came_from, goal)
                //        remove current from openset
                //        add current to closedset

                foreach (Vector2i neighbour in Neighbours(current.Pos, unit.Size, map, unit))
                {
                    AStarNode neighbourNode = new AStarNode(neighbour, shortestEstimate(from, neighbour), -1, current);

                    if (Find(neighbour, closed) != null)
                    {
                        if (isDebug) Debug.Log("Step " + i + " neighbour " + neighbour + " skipped as closed");
                        continue;
                    }

                    if (isDebug) Debug.Log("Step " + i + " neighbour " + neighbour + " is ok");

                    //        for each neighbor in neighbor_nodes(current)
                    //            if neighbor in closedset
                    //                continue
                    //            tentative_g_score := g_score[current] + dist_between(current,neighbor)

                    float newGScore = current.DistanceFromStart + Vector2.Distance(current.Pos.ToVector2, neighbour.ToVector2);
                    if (Find(neighbour, opened) == null || newGScore < neighbourNode.DistanceFromStart)
                    {
                        if (isDebug) Debug.Log("Step " + i + " neighbour " + neighbour + " gonna be opened");
                        // + " found better path | old " + neighbourNode.DistanceFromStart + " -> " + newGScore + " new");

                        neighbourNode.DistanceFromStart = newGScore;
                        neighbourNode.DistanceToFinish = neighbourNode.DistanceFromStart + shortestEstimate(neighbour, to);

                        if (Find(neighbour, opened) == null)
                        {
                            if (isDebug) Debug.Log("Step " + i + " neighbour " + neighbour + " added to openList");
                            insertSorted(opened, neighbourNode);
                        }
                        else
                        {
                            if (isDebug) Debug.Log("Step " + i + " neighbour " + neighbour + " allready in open list");
                        }

                    }
                    //            if neighbor not in openset or tentative_g_score < g_score[neighbor] 
                    //                came_from[neighbor] := current
                    //                g_score[neighbor] := tentative_g_score
                    //                f_score[neighbor] := g_score[neighbor] + heuristic_cost_estimate(neighbor, goal)
                    //                if neighbor not in openset
                    //                    add neighbor to openset

                }

            }

            if (okFinish)
            {
                if (isDebug) Debug.Log("Pathfinding finished ok from " + from + " to " + to + ", size " + unit.Size + " closed size = " + closed.Count + " opened size = " + opened.Count + " in " + i + " iterations");

                List <Vector2i> directions = reconstructPathByDirections(finalNode);

                if (isDebug) Debug.Log("Length = " + directions.Count);

                return directions;
            }
            else
            {
                if (isDebug) Debug.LogError("Pathfinding failed from " + from + " to " + to + ", size " + unit.Size + " closed size = " + closed.Count + " opened size = " + opened.Count);
                return null;
            }
        }

        public static List<Vector2i> AStarRun(Map map, Unit fromUnit, Unit toUnit, float radius, Unit unit) // for left bottom corner
        {
            Vector2i from = fromUnit.Position.BottomLeftOnMapMap;
            Vector2i to = toUnit.Position.BottomLeftOnMapMap;


            List<AStarNode> closed = new List<AStarNode>();
            List<AStarNode> opened = new List<AStarNode>();
            opened.Add(new AStarNode(from, 0.0f, shortestEstimate(from, to), null));
            
            int maxIter = 20000;//int.MaxValue;

            bool okFinish = false;
            AStarNode finalNode = null;

            int i;
            for (i = 0; i < maxIter && opened.Count > 0; i++)
            {

                if (isDebug)
                {
                    string str = "List (" + opened.Count + ") = ";
                    for (int j = 0; j < opened.Count; j++)
                        str += "\n\t" + opened[j];
                    Debug.Log(str);
                }

                AStarNode current = opened[0];
                if (GameController.DistanceMap.GetRangeBetween(current.Pos, fromUnit.Size, to, toUnit.Size) < radius)
                {
                    okFinish = true;
                    finalNode = current;
                    break;

                }

                //if (Vector2.Distance(current.Pos.ToVector2, to.ToVector2) < 2)
                if (isDebug) Debug.Log("Step " + i + " point main " + current + " target = " + to);


                opened.Remove(current);
                closed.Add(current);

                //    while openset is not empty
                //        current := the node in openset having the lowest f_score[] value
                //        if current = goal
                //            return reconstruct_path(came_from, goal)
                //        remove current from openset
                //        add current to closedset

                foreach (Vector2i neighbour in Neighbours(current.Pos, unit.Size, map, unit))
                {
                    AStarNode neighbourNode = new AStarNode(neighbour, shortestEstimate(from, neighbour), -1, current);

                    if (Find(neighbour, closed) != null)
                    {
                        if (isDebug) Debug.Log("Step " + i + " neighbour " + neighbour + " skipped as closed");
                        continue;
                    }

                    if (isDebug) Debug.Log("Step " + i + " neighbour " + neighbour + " is ok");

                    //        for each neighbor in neighbor_nodes(current)
                    //            if neighbor in closedset
                    //                continue
                    //            tentative_g_score := g_score[current] + dist_between(current,neighbor)

                    float newGScore = current.DistanceFromStart + Vector2.Distance(current.Pos.ToVector2, neighbour.ToVector2);
                    if (Find(neighbour, opened) == null || newGScore < neighbourNode.DistanceFromStart)
                    {
                        if (isDebug) Debug.Log("Step " + i + " neighbour " + neighbour + " gonna be opened");
                        // + " found better path | old " + neighbourNode.DistanceFromStart + " -> " + newGScore + " new");

                        neighbourNode.DistanceFromStart = newGScore;
                        neighbourNode.DistanceToFinish = neighbourNode.DistanceFromStart + shortestEstimate(neighbour, to);

                        if (Find(neighbour, opened) == null)
                        {
                            if (isDebug) Debug.Log("Step " + i + " neighbour " + neighbour + " added to openList");
                            insertSorted(opened, neighbourNode);
                        }
                        else
                        {
                            if (isDebug) Debug.Log("Step " + i + " neighbour " + neighbour + " allready in open list");
                        }

                    }
                    //            if neighbor not in openset or tentative_g_score < g_score[neighbor] 
                    //                came_from[neighbor] := current
                    //                g_score[neighbor] := tentative_g_score
                    //                f_score[neighbor] := g_score[neighbor] + heuristic_cost_estimate(neighbor, goal)
                    //                if neighbor not in openset
                    //                    add neighbor to openset

                }

            }

            if (okFinish)
            {
                if (isDebug) Debug.Log("Pathfinding finished ok from " + from + " to " + to + ", size " + unit.Size + " closed size = " + closed.Count + " opened size = " + opened.Count + " in " + i + " iterations");

                List<Vector2i> directions = reconstructPathByDirections(finalNode);

                if (isDebug) Debug.Log("Length = " + directions.Count);

                return directions;
            }
            else
            {
                if (isDebug) Debug.LogError("Pathfinding failed from " + from + " to " + to + ", size " + unit.Size + " closed size = " + closed.Count + " opened size = " + opened.Count);
                return null;
            }
        }

        private static List<Vector2i> reconstructPathByDirections(AStarNode lastNode)
        {
            List<Vector2i> points = reconstructPathByPoints(lastNode);

            for (int i = 0; i < points.Count - 1; i++)
            {
                points[i] = points[i + 1] - points[i];
            }

            points.RemoveAt(points.Count - 1);

            return points;
        }

        private static List<Vector2i> reconstructPathByPoints(AStarNode lastNode)
        {
            List<Vector2i> ret = new List<Vector2i>();

            //Debug.Log("Reconstruct Point " + lastNode.Pos + " parent = " + (lastNode.ParentNode == null ? "null" : lastNode.ParentNode.Pos.ToString()));

            ret.Add(lastNode.Pos);

            if (!GameController.Map.IsInBounds(lastNode.Pos))
                Debug.LogException(new System.Exception("Node is out of map! " + lastNode));

            if (lastNode.ParentNode != null)
                ret.InsertRange(0, reconstructPathByPoints(lastNode.ParentNode));

            return ret;
            //function reconstruct_path(came_from, current_node)
            //    if current_node in came_from
            //        p := reconstruct_path(came_from, came_from[current_node])
            //        return (p + current_node)
            //    else
            //        return current_node
        }
    }

    private static float shortestEstimate(Vector2i from, Vector2i to)
    {
        int dx = Mathf.Abs(from.x - to.x);
        int dy = Mathf.Abs(from.y - to.y);
        int diagonalSteps = Mathf.Min(dx, dy);
        int straightSteps = Mathf.Max(dx, dy) - diagonalSteps;

        float ret = Mathf.Sqrt(2.0f) * diagonalSteps + 1.0f * straightSteps;
        //Debug.Log("Distance from " + from + " to " + to + " = " + ret);
        return ret;
    }
}
