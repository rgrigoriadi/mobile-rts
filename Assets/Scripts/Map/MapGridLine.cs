﻿using UnityEngine;
using System.Collections;

public class MapGridLine : MonoBehaviour {
	
	private const float lineWidth = 0.05f;
	
	public void SetHorizontal(int y, float length) 
	{
		transform.localPosition = new Vector3(length / 2.0f, y, transform.localPosition.z);
		setSize(new Vector3(length, lineWidth, lineWidth));
	}
	
	public void SetVertical(int x, float length) 
	{
		transform.localPosition = new Vector3(x, length / 2.0f, transform.localPosition.z);
		setSize(new Vector3(lineWidth, length, lineWidth));
	}
	
	private void setSize(Vector3 newSize) 
	{
		Vector3 size = GetComponent<MeshRenderer>().bounds.size;
		Vector3 scale = transform.localScale;
		transform.localScale = new Vector3(scale.x * newSize.x / size.x, scale.y * newSize.y / size.y, scale.z * newSize.z / size.z);
	}
}
