﻿using UnityEngine;
using System.Collections;

public abstract class AMapMesh : MonoBehaviour // выполняет работу с мешем карты, крепится к нему же (должен быть 1 на карте
{
    // угол карты находится в координате 0, 0 и простирается по Х и У в положительных направлениях

    public abstract void LoadMap(AMapLevel level); // sizeInCells - размер карты в клетках, density 

    //public abstract float GetHeight(Vector2 position); // возвращает высоту точки, расположенной по X и Y

}
