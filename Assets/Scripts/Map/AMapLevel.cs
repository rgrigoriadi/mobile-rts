﻿using UnityEngine;
using System.Collections;

public abstract class AMapLevel
{
    public const int DEFAULT_LEVEL = -1;

    public Vector2i GetSizeInCells()
    {
        int[,] heights = GetHeightMap();
        int density = GetDensity();
        return new Vector2i(heights.GetLength(0) / density, heights.GetLength(1) / density);
    }

    public abstract int[,] GetHeightMap();
    public abstract int GetDensity();
}
