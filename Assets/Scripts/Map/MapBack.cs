﻿using UnityEngine;
using System.Collections;

public class MapBack : MonoBehaviour {

    public Color color;

	public void SetSize (Vector2i size) 
	{
        GetComponent<Renderer>().material.color = color;

		Vector2 bounds = (Vector2) GetComponent<MeshRenderer>().bounds.size;
		Vector2 scaleCoef = new Vector2(size.x / bounds.x * 1.0f, size.y / bounds.y * 1.0f);
		transform.localScale = new Vector3(transform.localScale.x * scaleCoef.x, transform.localScale.y * scaleCoef.y, transform.localScale.z);
		
		bounds = (Vector2) GetComponent<MeshRenderer>().bounds.size;
		transform.position = new Vector3(bounds.x / 2.0f, bounds.y / 2.0f, transform.position.z); 
	}
}
