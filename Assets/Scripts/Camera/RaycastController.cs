﻿using UnityEngine;
using System.Collections;

public class RaycastController : MonoBehaviour {

    public static int LAYER_UNIT = 10;
    public static int LAYER_MAP = 9;
    public static int LAYER_ZERO = 8;

    public static int LAYER_GUI = 11;

    public static int LAYER_OBJECTS_MASK { get {
            return GetMask(LAYER_UNIT) | GetMask(LAYER_MAP);
        }
    }

    public static int GetMask(int layer) {
        return 1 << (layer);
    }

    public static bool RaycastObject(Ray ray, out RaycastHit hit){
        return Raycast(ray, GetMask(LAYER_UNIT), out hit);
    }

    public static bool RaycastMap(Ray ray, out RaycastHit hit) {
        return Raycast(ray, GetMask(LAYER_MAP), out hit);
    }

    public static bool RaycastMapAndObject(Ray ray, out RaycastHit hit) {
        return Raycast(ray, LAYER_OBJECTS_MASK, out hit);
    }

    public static bool RaycastZeroLevel(Ray ray, out RaycastHit hit) {
        return Raycast(ray, GetMask(LAYER_ZERO), out hit);
    }

    public static bool RaycastGUI(Vector2 posOnScreen, out RaycastHit hit)
    {
        Ray ray = GameController.GUICamera.ScreenPointToRay(posOnScreen);

        return Raycast(ray, GetMask(LAYER_GUI), out hit);
    }

    private static bool Raycast(Ray ray, int mask, out RaycastHit hit)
    {
        return Physics.Raycast(ray, out hit, float.PositiveInfinity, mask);
    }

    public static Ray CursorToRay(Vector2 pointOnScreen)
    {
        return GameController.Camera.camera.ScreenPointToRay(pointOnScreen);
    }

    public static bool IsGUIHit(Vector2 positionOnScreen)
    {
        RaycastHit guihit = new RaycastHit();
        return RaycastGUI(positionOnScreen, out guihit);
    }
}