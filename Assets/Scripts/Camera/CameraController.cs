﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	
	private float maxHeigth = 13.0f;

    private Vector2i mapSize;

    private float currentInertialScrollSpeed = 0.0f; // Squares per second
    private float linearCoef = 100.0f;
    private float newInertialScrollSpeed(float oldSpeed, float deltatime)
    {
        float ret;
        if (oldSpeed > 0)
        {
            ret = oldSpeed - deltatime * linearCoef;
            if (ret <= 0.01f)
                ret = 0;
        }
        else if (oldSpeed < 0)
        {
            ret = oldSpeed + deltatime * linearCoef;
            if (ret >= -0.01f)
                ret = 0;
        }
        else 
            ret = 0;

        return ret;
    }

	public float MaxHeight { get { return maxHeigth; } }
	public float Angle { get { return transform.rotation.eulerAngles.x; } }

    public float LeftBound { get { return 0.0f; } }
    public float RightBound { get { return mapSize == null ? 0.0f : mapSize.x; } }
	
	void Start () {
	
	}
	
	void Update () {
        if (currentInertialScrollSpeed != 0)
        {
            X += currentInertialScrollSpeed * Time.deltaTime;
            currentInertialScrollSpeed = newInertialScrollSpeed(currentInertialScrollSpeed, Time.deltaTime);
        }
    }
	
	public float Y { set {
			transform.position = new Vector3(transform.position.x, 
                                            value + MaxHeight * Mathf.Sin (Angle * Mathf.PI / 180.0f),
                                            - MaxHeight * Mathf.Cos (Angle * Mathf.PI / 180.0f)); } 
    }
	
	public float X { 
        get { return transform.position.x; }

		set {
			Vector3 pos = transform.position;
			pos.x = value < LeftBound ? LeftBound : value > RightBound ? RightBound : value;
			transform.position = pos; }
	}
	
	public void InitCam (Vector2i mapSize) 
	{
		Y = mapSize.y / 2.0f;
		X = 0.0f;

        this.mapSize = mapSize;
	}

    public void RunInertialScroll(float initSpeed) // include direction
    {
        currentInertialScrollSpeed = initSpeed;
    }

    public void StopInertialScroll()
    {
        currentInertialScrollSpeed = 0.0f;
    }
}
